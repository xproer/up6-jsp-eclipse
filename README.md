# up6-jsp-eclipse
泽优Web大文件上传控件（up6）for jsp-eclipse示例

## 环境支持
文件大小：2T  
浏览器支持：IE6,IE7,IE8,IE9,IE10,IE11,火狐(Firefox),谷歌(Chrome),Edge,Safari,欧朋(Opera),奇安信,龙芯,猎豹,搜狗,红莲花,360极速,360安全,QQ浏览器,傲游浏览器(Maxthon),2345浏览器,115浏览器,UC浏览器,世界之窗浏览器,百度浏览器  
服务器支持：Windows,macOS,Linux,信创国产化系统  
系统支持：Windows,macOS,Linux,中标麒麟，银河麒麟，统信UOS  
数据库支持：SQL Server,MySQL,Oracle,达梦,人大金仓,MongoDB  
信创及国产化支持：X86(海光，兆芯)，arm(鲲鹏，飞腾)，龙芯(mips,LoongArch)  
功能：文件，文件夹上传下载，断点续传，[加密传输(AES,SM4国密)](https://www.ixigua.com/7222890207396233768)，[压缩传输](https://www.ixigua.com/7222898301190963768)，[加密存储](http://www.ncmem.com/doc/view.aspx?id=d16c5f68f8294f558873bd0a748538a6)  

## 安装控件
[安装Windows控件](http://www.ncmem.com/doc/view.aspx?id=9bbff9517a9d455abe801d3b207e0dfe)  
[安装macOS(x86)控件](http://www.ncmem.com/doc/view.aspx?id=37e5c00081814e46a7a981695c2fbc98)  
[安装Linux-deb控件](http://www.ncmem.com/doc/view.aspx?id=eaa329cd1fec4b35a3d5e13cc73190d3)  
[安装Linux-rpm控件](http://www.ncmem.com/doc/view.aspx?id=0b929c2a74254d21b21dac0c8c691af4)  
[卸载控件](http://www.ncmem.com/doc/view.aspx?id=1d22095c0f47421b9656b0c783691274)  

## 云对象存储
[阿里云](http://www.ncmem.com/doc/view.aspx?id=04aa1f23dd754d6cafa91785ca3ef073)  
腾讯云  
百度云  
七牛云  
亚马逊云  
[华为云](http://www.ncmem.com/doc/view.aspx?id=f9dcadee51c44b5fb65aaa1c2b0b5b7f)  
[MinIO](http://www.ncmem.com/doc/view.aspx?id=63aa7078c7db48a4a3c5a17cff690e44)  
[FastDFS](http://www.ncmem.com/doc/view.aspx?id=b517fbf6ca3f4df3b162be9f6695f5ca)

## 相关资源
[官方网站](http://www.ncmem.com/index.aspx)  
[控件包](http://www.ncmem.com/webapp/up6/pack.aspx)  
[示例下载](http://www.ncmem.com/webapp/up6/versions.aspx)  
[在线文档](http://www.ncmem.com/doc/view.aspx?id=653253c5b16243f4835469e82c2c1146)  
[产品比较](http://www.ncmem.com/doc/view.aspx?id=0A9415192654401BAEED9370160DB197)  
[产品介绍](http://www.ncmem.com/doc/view.aspx?id=960EE3FE37AF42FD98249C5CC2533E83)  
[白皮书](https://drive.weixin.qq.com/s?k=ACoAYgezAAwqb51JS5)  
[产品源码文档](http://www.ncmem.com/webapp/up6/purchase.aspx?type=code)  
[个人版报价单](https://drive.weixin.qq.com/s?k=ACoAYgezAAwoNfBY7s)  
[订阅版报价单](https://drive.weixin.qq.com/s?k=ACoAYgezAAwD7OMyBy)  
[政企版报价单](http://www.ncmem.com/webapp/up6/purchase.aspx?type=ent)  
[年费版报价单](http://www.ncmem.com/webapp/up6/purchase.aspx?type=year)  

## 测试
[Eclipse测试](http://www.ncmem.com/doc/view.aspx?id=b6385cd5cf974e5ebf7f0444559fc2b6)  
[SpringBoot测试](http://www.ncmem.com/doc/view.aspx?id=7768eec9284b48e3abe08f032f554ea2)  
[SM4加密存储](http://www.ncmem.com/doc/view.aspx?id=92100e8f61d24aa2aa0248b3fade4ce9)  
[使用PostpreSQL数据库](http://www.ncmem.com/doc/view.aspx?id=497f06896c2b40079e40e5ca085cf5c4)  
[使用Oracle数据库](http://www.ncmem.com/doc/view.aspx?id=e8558626b37340e9966033fb6e5c559c)  
[使用MySQL数据库](http://www.ncmem.com/doc/view.aspx?id=ba337111262147b7a9d59e44f6a1d04a)  
[使用SQL Server数据库](http://www.ncmem.com/doc/view.aspx?id=cb3983e09ca54077a4d37fa6fecb570f)  
[使用MinIO存储](http://www.ncmem.com/doc/view.aspx?id=0d58a1a96af0425a8c6bd5f730a31c7c)  
[使用FastDFS存储](http://www.ncmem.com/doc/view.aspx?id=e38ad325c53640fda2ba2656d266a6b6)  
[使用阿里云对象存储(OSS)](http://www.ncmem.com/doc/view.aspx?id=4d1bb89a95044a57a612dc3662be452c)  
[使用华为云对象存储(OBS)](http://www.ncmem.com/doc/view.aspx?id=6257ea4a634045a9a86a37ef61b07e58)  
[使用Resin服务器](http://www.ncmem.com/doc/view.aspx?id=e1cc4a412f6c40fba0807a1ee9ca5528)  
[使用Tomcat服务器](http://www.ncmem.com/doc/view.aspx?id=d5a0b390d72c4ab782fd88f7aab7613d)

## 解决方案
[党政](https://drive.weixin.qq.com/s?k=ACoAYgezAAw8CMjliR)  
[央企](https://drive.weixin.qq.com/s?k=ACoAYgezAAwPvHk9ge)  
[国防军工](https://drive.weixin.qq.com/s?k=ACoAYgezAAwomnBHGP)  
[档案管理](https://drive.weixin.qq.com/s?k=ACoAYgezAAwYeg7NbY)  
[金融](https://drive.weixin.qq.com/s?k=ACoAYgezAAw7bPYO40)  
[地产](https://drive.weixin.qq.com/s?k=ACoAYgezAAwB9Bc6ba)  
[工程管理](https://drive.weixin.qq.com/s?k=ACoAYgezAAw0VdLVsH)  
[教育](https://drive.weixin.qq.com/s?k=ACoAYgezAAwMm9JsV5)  
[医疗](https://drive.weixin.qq.com/s?k=ACoAYgezAAw19MkH5b)  
[广告设计](https://drive.weixin.qq.com/s?k=ACoAYgezAAwKjwJTD0)

## 教程
[加密传输](https://www.ixigua.com/7222890207396233768)  
[加密存储](https://www.ixigua.com/7234432904405713442)  
[压缩传输](https://www.ixigua.com/7222898301190963768)  
[音视频接口](http://www.ncmem.com/doc/view.aspx?id=d23f44b0fe2f45d0a27ebdcffce08b10)  
[断点续传](https://www.ixigua.com/7203544091609694772)  
[文件夹上传](https://www.ixigua.com/7203551115256791591)  
[Windows控件安装](https://www.ixigua.com/7230757235625263674)  
[macOS控件安装](https://www.ixigua.com/7230759875431629371)  
[Linux-deb控件安装](https://www.ixigua.com/7233320878690599481)  
[Linux-rpm控件安装](https://www.ixigua.com/7232195342228259328)

## 控件源代码
[下载控件源代码](https://drive.weixin.qq.com/s?k=ACoAYgezAAwbdKCskc)  
[下载授权生成器](https://drive.weixin.qq.com/s?k=ACoAYgezAAwTIcFph1)  
[Windows编译教程](https://www.ixigua.com/7202866235439219240)  
[macOS编译教程](https://www.ixigua.com/7203598398442701312)  
[Linux编译教程](https://www.ixigua.com/7226301779485393467)  
[CentOS编译教程](https://www.ixigua.com/7226301779485393467)  
[优麒麟编译教程](https://www.ixigua.com/7226301779485393467)  
[中标麒麟编译教程](https://www.ixigua.com/7226983742462657064)  
[银河麒麟编译教程](https://www.ixigua.com/7226983742462657064)  
[统信UOS编译教程](https://www.ixigua.com/7226983742462657064)

## 谁在使用
### 谁在下载源代码？
为您提供产品完整源代码，可集成到您的多个产品和多个项目中使用，帮助您降低产品研发成本，缩短项目上线时间。  
![吉利控股集团](res/code-geely.png)
![小红书](res/code-xiaohongshu.png)
![合肥欣奕华](res/code-sineva.png)
![夏门中闽投资](res/code-xmzm.png)
![威尔视觉](res/code-vrviu.png)
![伟立机器人](res/code-welllih.png)
![珞珈众恒](res/code-allprs.png)
![比瑞科技（深圳）](res/code-bmz.png)
![中兴银行](res/code-citicbank.png)
![中任银兴](res/code-cnzryx.png)
![三五互联](res/code-35.png)
![磐诺仪器](res/code-pannatek.png)
![2024-11](res/code-2024-11.png)
![源代码下载记录](res/code-1m.png)
### 谁在使用授权器？
近期产品授权码生成器下载记录，授权码生成器主要生成产品授权码，为个人和企业提供自主授权生成服务  
![授权器下载记录](res/lic.png)
### 谁在查看工程文档？
为您免费提供近期的工程文档查看记录，这个也是日更，尽量保证每天为大家更新一次，便于大家实时了解最新的情况，但是如果做不到也希望大家能够理解  
![比亚迪集团](res/doc-byd.png)
![中国外运股份有限公司](res/doc-sinotrans.png)
![中国核工业](res/doc-cni22.png)
![河南交通投资](res/doc-hnjttz.png)
![东软集团](res/doc-neusoft.png)
![高凌信息](res/doc-comleader.png)
![熙软科技](res/doc-xisofttec.png)
![百富计算机](res/doc-pax.png)
![超图软件](res/doc-supermap.png)
![恒歌科技](res/doc-henggetec.png)
![smart汽车](res/doc-smart.png)
![上海交通大学](res/doc-shsmu.png)
### 谁在申请源码版？
![源码版下载申请记录](res/src-1.png)
![源码版下载申请记录](res/src-2.png)
![源码版下载申请记录](res/src-3.png)
![源码版下载申请记录](res/src-4.png)
![源码版下载申请记录](res/src-5.png)
![源码版下载申请记录](res/src-6.png)
![源码版下载申请记录](res/src-7.png)
![源码版下载申请记录](res/src-8.png)
![源码版下载申请记录](res/src-9.png)
![源码版下载申请记录](res/src-10.png)
![源码版下载申请记录](res/src-11.png)
![源码版下载申请记录](res/src-12.png)
![源码版下载申请记录](res/src-13.png)
![源码版下载申请记录](res/src-14.png)
![云晖航空科技](res/src-15.png)
### 技术支持
![中国港湾](res/tec-1.png)
![金润方舟](res/tec-2.png)
![北京兴油](res/tec-3.png)
![济南](res/tec-4.png)

## 联系我们
QQ：1269085759(技术)  
QQ：1085617561(商务)  
QQ群：[374992201](http://shang.qq.com/wpa/qunwpa?idkey=00e554f5a80bf0209006b6b172a73ee3b34d27ef708222de8a374121ef9775ab)  
微信：13235643658  
电话：13235643658  
邮箱：1269085759@qq.com  
官网：[http://www.ncmem.com](http://www.ncmem.com)

## 标杆案例
[中国港湾工程有限责任公司](https://www.ixigua.com/7276384414878958136)  
[中国中车股份有限公司](https://www.ixigua.com/7232111895313023520?id=7277771874954805814)  
[中国石油天然气集团有限公司](https://www.ixigua.com/7232111895313023520?id=7278506605073826341&logTag=e48f8b868b9849830e1f)  
[中国长江电力股份有限公司](https://www.ixigua.com/7232111895313023520)  
[中国中信集团有限公司](https://www.ixigua.com/7282205969273061945)  
[上海通用](https://www.ixigua.com/7331303913619980800)  
[一汽大众](https://www.ixigua.com/7331676432294707738)  
[新华三技术有限公司](https://www.ixigua.com/7339813499960623679)  
[深圳市爱德数智科技股份有限公司](https://www.ixigua.com/7330489273650676235)  
[金润方舟科技股份有限公司](https://www.ixigua.com/7329817304764678656)  
[黑龙江省监狱管理局](https://www.ixigua.com/7338676497323917864)