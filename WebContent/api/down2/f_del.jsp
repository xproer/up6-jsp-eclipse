<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="down2.biz.DnFile" %>
<%
out.clear();
/*
	从down_files中删除文件下载任务
	更新记录：
		2015-05-13 创建
		2016-07-29 更新。
*/
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

String fid = request.getParameter("id");
String uid = request.getParameter("uid");
String cbk = request.getParameter("callback");//jsonp

if (	StringUtils.isEmpty(uid)
	||	StringUtils.isBlank(fid)
	)
{
	out.write(cbk + "({\"value\":null})");
	return;
}
DnFile.build().Delete(fid, uid);
out.write(cbk+"({\"value\":1})");%>