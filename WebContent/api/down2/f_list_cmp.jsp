<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="down2.biz.DnFile" %>
<%
out.clear();
/*
	列出所有已经上传完的文件和文件夹列表
	主要从up6_files中读取数据
	更新记录：
		2012-05-24 完善
		2012-06-29 增加创建文件逻辑，
		2016-07-29 更新
*/
String path = request.getContextPath();
String uid 	= request.getParameter("uid");
String cbk  = request.getParameter("callback");//jsonp

if (!StringUtils.isEmpty(uid))
{
	String json = DnFile.build().all_complete(uid);
	if(!StringUtils.isBlank(json))
	{
		System.out.println("上传文件列表："+json);
		json = URLEncoder.encode(json,"utf-8");
		json = json.replace("+","%20");
		out.write(cbk + "({\"value\":\""+json+"\"})");
		return;
	}
}
out.write(cbk+"({\"value\":null})");
%>