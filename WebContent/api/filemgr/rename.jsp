<%@ page language="java" import="filemgr.PageFileMgr" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);
    PageFileMgr mgr = new PageFileMgr();

    String id      = web.reqString("id");
    boolean folder = web.reqToBool("fdTask");
    String pathRel = web.reqStringDecode("pathRel");
    String nameNew = web.reqStringDecode("nameLoc");

    JSONObject v = null;
    if (folder)
    {
        v = mgr.rename_folder(id,pathRel,nameNew);
    }
    else
    {
        v = mgr.rename_file(id,pathRel,nameNew);
    }

    out.write(v.toString());
%>