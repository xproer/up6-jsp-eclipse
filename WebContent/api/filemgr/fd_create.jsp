<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.FolderSchema" %>
<%@	page import="up6.biz.PathBuilderUuid" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.net.URLEncoder" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String id       = web.queryString("id");
    String pid      = web.queryString("pid");
    String pidRoot  = web.queryString("pidRoot");
    if( StringUtils.isBlank(pidRoot)) pidRoot = pid;//父目录是根目录
    String uid      = web.queryString("uid");
    String lenLoc   = web.queryString("lenLoc");
    String sizeLoc  = web.queryString("sizeLoc");
    String pathLoc  = web.reqStringDecode("pathLoc");
    String pathRel  = web.reqStringDecode("pathRel");
    String callback = web.queryString("callback");//jsonp


    //参数为空
    if (StringUtils.isBlank(id)
            || StringUtils.isBlank(uid)
            || StringUtils.isBlank(pathLoc))
    {
        web.toContent(callback + "({\"value\":null})");
        return;
    }

    FileInf fileSvr = new FileInf();
    fileSvr.id      = id;
    fileSvr.pid     = pid;
    fileSvr.pidRoot = pidRoot;
    fileSvr.fdChild = false;
    fileSvr.fdTask  = true;
    fileSvr.uid     = uid;
    fileSvr.nameLoc = PathTool.getName(pathLoc);
    fileSvr.pathLoc = pathLoc;
    fileSvr.pathRel = PathTool.combin(pathRel, fileSvr.nameLoc);
    fileSvr.lenLoc  = Long.parseLong(lenLoc);
    fileSvr.sizeLoc = sizeLoc;
    fileSvr.deleted = false;
    fileSvr.nameSvr = fileSvr.nameLoc;

    //检查同名目录
		/*DbFolder df = new DbFolder();
		if (df.exist_same_folder(fileSvr.nameLoc, pid))
		{
			JSONObject o = new JSONObject();
			o.put("value","");
			o.put("ret", false);
			o.put("code", "102");
		    String js = callback + String.format("(%s)", o.toString());
		    web.toContent(js);
		    return;
		}*/

    //生成存储路径
    PathBuilderUuid pb = new PathBuilderUuid();
    try {
        fileSvr.pathSvr = pb.genFolder(fileSvr);
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");
    PathTool.createDirectory(fileSvr.pathSvr);

    //保存层级结构
    FolderSchema fs = new FolderSchema();
    fs.create(fileSvr);

    //添加到数据表
    DBFile db = DBFile.build();
    //添加到根目录
    if(StringUtils.equals(pathRel,"/")) db.Add(fileSvr);
    //添加到子目录
    else DbFolder.build().add(fileSvr);

    up6_biz_event.folder_create(fileSvr);

    Gson g = new Gson();
    String json = g.toJson(fileSvr);
    try {
        json = URLEncoder.encode(json,"utf-8");
    } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    json = json.replace("+","%20");

    JSONObject ret = new JSONObject();
    ret.put("value",json);
    ret.put("ret",true);
    json = callback + String.format("(%s)",ret.toString());//返回jsonp格式数据。
    out.write(json);
%>