<%@ page language="java" import="net.sf.json.JSONObject" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.database.DbFolder" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String data = web.queryString("data");
    data = PathTool.url_decode(data);
    JSONObject fd = JSONObject.fromObject(data);

    DbFolder df = new DbFolder();

    out.write(df.build_path(fd).toString());
%>