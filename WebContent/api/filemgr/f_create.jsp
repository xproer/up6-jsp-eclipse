<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.PathBuilderUuid" %>
<%@	page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.utils.ConfigReader" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.net.URLEncoder" %>
<%
    out.clear();
    /*
        所有单个文件均以md5模式存储。
        更新记录：
            2022-12-11 md5验证方法更新
            2017-07-13 取消生成id操作
            2016-04-09 完善逻辑。
            2016-01-07 返回值改为JSON
            2015-07-30 取消文件夹层级结构存储规则，改为使用日期存储规则，文件夹层级结构仅保存在数据库中。
            2012-06-29 增加创建文件逻辑，
            2012-05-24 完善
    */
    WebBase web = new WebBase(pageContext);

    String pid      = web.queryString("pid");
    String pidRoot  = web.queryString("pidRoot");
    if(StringUtils.isBlank(pidRoot)) pidRoot = pid;//当前文件夹是根目录
    String id		= web.queryString("id");
    String md5 		= web.queryString("md5");
    String uid 		= web.queryString("uid");
    String lenLoc 	= web.queryString("lenLoc");//数字化的文件大小。12021
    String sizeLoc 	= web.queryString("sizeLoc");//格式化的文件大小。10MB
    String blockSize = web.queryString("blockSize");//块大小
    int blockSizeSec = web.reqInt("blockSizeSec");//块加密大小
    String callback = web.queryString("callback");
    String pathLoc	= web.reqStringDecode("pathLoc");
    String pathRel  = web.reqStringDecode("pathRel");
    String encryptAgo = web.reqString("encryptAgo");
    Long lenLocSec = web.reqLong("lenLocSec");//文件加密大小

    //参数为空
    if (	StringUtils.isBlank(md5)
            && StringUtils.isBlank(uid)
            && StringUtils.isBlank(sizeLoc))
    {
        web.toContent(callback + "({\"value\":null})");
        return;
    }

    FileInf fileSvr= new FileInf();
    fileSvr.id = id;
    fileSvr.pid = pid;
    fileSvr.pidRoot = pidRoot;
    boolean isRootFile = StringUtils.equals(pathRel,"/");
    fileSvr.fdChild = !isRootFile;
    fileSvr.uid = uid;
    fileSvr.nameLoc = PathTool.getName(pathLoc);
    fileSvr.nameSvr = fileSvr.nameLoc;
    fileSvr.pathLoc = pathLoc;
    fileSvr.pathRel = PathTool.combin(pathRel, fileSvr.nameLoc);
    fileSvr.lenLoc = Long.parseLong(lenLoc);
    fileSvr.lenLocSec = lenLocSec;//
    fileSvr.sizeLoc = sizeLoc;
    fileSvr.deleted = false;
    fileSvr.md5 = md5;
    fileSvr.blockSize=Integer.parseInt(blockSize);
    fileSvr.blockSizeSec = blockSizeSec;
    fileSvr.encrypt = ConfigReader.storageEncrypt();//存储加密
    fileSvr.encryptAgo = encryptAgo;//加密算法，sm4,aes

    //所有单个文件均以uuid/file方式存储
    PathBuilderUuid pb = new PathBuilderUuid();
    try {
        fileSvr.pathSvr = pb.genFile(fileSvr.uid,fileSvr);
    } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
    }
    fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");


    //同名文件检测
		/*DbFolder df = new DbFolder();
		if (df.exist_same_file(fileSvr.nameLoc,pid))
		{
		    String data = callback + "({'value':'','ret':false,'code':'101'})";
		    web.toContent(data);
		    return;
		}*/

    DBFile db = DBFile.build();
    FileInf fileExist = db.exist_file(md5);
    //数据库已存在相同文件，且有上传进度，则直接使用此信息
    if(fileExist != null)
    {
        fileSvr.nameSvr  = fileExist.nameSvr;
        fileSvr.pathSvr  = fileExist.pathSvr;
        fileSvr.perSvr   = fileExist.perSvr;
        fileSvr.lenSvr   = fileExist.lenSvr;
        fileSvr.complete = fileExist.complete;
        fileSvr.encrypt = fileExist.encrypt;
        fileSvr.encryptAgo = fileExist.encryptAgo;
        fileSvr.lenLocSec = fileExist.lenLocSec;
        fileSvr.blockSize = fileExist.blockSize;
        fileSvr.blockSizeSec = fileExist.blockSizeSec;
        fileSvr.object_key = fileExist.object_key;
        db.Add(fileSvr);

        //触发事件
        up6_biz_event.file_create_same(fileSvr);
    }//此文件不存在
    else
    {
        FileBlockWriter fw = ConfigReader.blockWriter();
        try {
			fileSvr.object_id = fw.make(fileSvr);
			fileSvr.object_key = fileSvr.ObjectKey();
		} catch (IOException e) {
			e.printStackTrace();
			JSONObject obj = new JSONObject();
	        obj.put("value", "");
	        obj.put("error", e.getMessage());
	        obj.put("ret", false);
	        out.write(callback + "(" + obj.toString() + ")");
	        return;
		}

        db.Add(fileSvr);
        //触发事件
        up6_biz_event.file_create(fileSvr);
    }

    //将路径转换成相对路径
    fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);

    Gson gson = new Gson();
    String json = gson.toJson(fileSvr);

    try {
        json = URLEncoder.encode(json,"UTF-8");
    } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }//编码，防止中文乱码
    json = json.replace("+","%20");
    json = callback + "({\"value\":\"" + json + "\",\"ret\":true})";//返回jsonp格式数据。
    out.write(json);%>