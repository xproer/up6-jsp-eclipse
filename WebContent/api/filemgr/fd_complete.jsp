<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.biz.FolderScaner" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBFile" %>
<%@	page import="up6.database.DbFolder" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String id		= web.reqString("id");
    String uid 		= web.reqString("uid");
    String pid      = web.reqString("pid");
    String pathRel	= web.reqStringDecode("pathRel");
    String cbk 		= web.reqString("callback");//jsonp
    int cover		= web.reqInt("cover");//是否覆盖
    int ret = 0;

    //参数为空
    if (!StringUtils.isBlank(id))
    {
        //是根目录
        boolean isRootDir = StringUtils.equals(pathRel,"/");
        FileInf folder=null;
        FileInf fdExist=null;

        //子目录
        if(!isRootDir)
        {
            folder = DbFolder.build().read(id);
            fdExist = DbFolder.build().read(folder.pathRel, id);

            //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
            if (fdExist != null) DbFolder.build().del(fdExist.id, uid);
        }//根目录
        else{
            folder = DBFile.build().read(id);
            fdExist = DBFile.build().read(folder.pathRel, id,uid);

            //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
            if (fdExist != null) DBFile.build().Delete(uid, fdExist.id);
        }

        if(fdExist !=null)
        {
            folder.id = fdExist.id;
            folder.pid = fdExist.pid;
            folder.pidRoot = fdExist.pidRoot;
        }

        //根节点
        FileInf root = new FileInf();
        root.id = folder.pidRoot;
        root.uid = folder.uid;
        //当前节点是根节点
        if( StringUtils.isBlank(root.id)) root.id = folder.id;

        FolderScaner fsd = new FolderScaner();
        //覆盖同路径项
        fsd.m_cover = fdExist!=null;
        fsd.save(folder);

        //上传完毕
        if(!isRootDir) DbFolder.build().complete(id,uid);
        //根目录
        else DBFile.build().complete(id);

        up6_biz_event.folder_post_complete(id);

        ret = 1;
    }
    out.write(cbk + "(" + ret + ")");
%>