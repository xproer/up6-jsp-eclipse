<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.database.DbFolder" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();
    WebBase web = new WebBase(pageContext);
    String pid = web.queryString("pid");
    String uid = web.reqString("uid");
    String pathRel = web.reqStringDecode("pathRel");
    if(!pathRel.endsWith("/")) pathRel+="/";
    //根目录
    DbFolder db = DbFolder.build();
    if (StringUtils.equals(pathRel,"/") ) out.write(db.loadRootData(uid,pid,pathRel));
    else out.write(db.loadChildData(uid,pid,pathRel));
%>