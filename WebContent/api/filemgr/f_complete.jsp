<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.PathTool" %>
<%@	page import="up6.utils.WebBase" %>
<%
    out.clear();
/*
	所有单个文件均以md5模式存储。
	更新记录：
	    2022-12-11 完善同名文件删除逻辑
*/
    WebBase web = new WebBase(pageContext);

    String md5 		= web.reqString("md5");
    String uid 		= web.reqString("uid");
    String id		= web.reqString("id");
    String pid 		= web.reqString("pid");
    String callback = web.reqString("callback");//jsonp
    int cover		= web.reqInt("cover");//是否覆盖
    String nameLoc	= web.reqString("nameLoc");//文件名称
    nameLoc			= PathTool.url_decode(nameLoc);

    //返回值。1表示成功
    int ret = 0;
    if ( !StringUtils.isBlank(id))
    {
        DBFile db = DBFile.build();
        db.complete(id);
        FileInf file = db.read(id);

        //覆盖同名文件-更新同名文件状态
        if(cover == 1) db.delete(file.pathRel, uid, id);

        up6_biz_event.file_post_complete(id);
        ret = 1;
    }
    out.write(callback + "(" + ret + ")");
%>