<%@ page language="java" import="net.sf.json.JSONObject" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@	page import="up6.database.DbFolder" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String name = web.reqStringDecode("nameLoc");
    String pid = web.reqString("pid");
    String uid = web.reqString("uid");
    String pidRoot = web.reqString("pidRoot");
    String pathRel = web.reqStringDecode("pathRel");
    boolean isRootDir = StringUtils.equals(pathRel,"/");
    pathRel = PathTool.combin(pathRel, name);

    DBConfig cfg = new DBConfig();
    DbFolder df = DbFolder.build();
    if (df.exist_same_folder(pathRel))
    {
        JSONObject ret = new JSONObject();
        ret.put("ret", false);
        ret.put("msg", "已存在同名目录");
        out.write( ret.toString() );
        return;
    }

    FileInf dir = new FileInf();
    dir.id = PathTool.guid();
    dir.pid = pid;
    dir.uid = uid;
    dir.pidRoot = pidRoot;
    dir.nameLoc = name;
    dir.nameSvr = name;
    dir.complete = true;
    dir.fdTask = true;
    dir.pathRel = pathRel;

    //根目录
    if (isRootDir)
    {
        DBFile.build().Add(dir);
    }//子目录
    else
    {
        DbFolder.build().add(dir);
    }
    JSONObject ret = JSONObject.fromObject(dir);
    ret.put("ret", true);
    out.write(ret.toString());
%>