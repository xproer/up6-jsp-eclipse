<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.model.DnFileInf" %>
<%@	page import="up6.sql.SqlTable" %>
<%@ page import="up6.sql.SqlWhere" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.util.List" %>
<%
	/**
	 * 加载未下载完的任务列表
	 */
    out.clear();

    WebBase web = new WebBase(pageContext);

    List<DnFileInf> files = SqlTable.build("down_files").reads(DnFileInf.build(),
            SqlWhere.build().eq("f_uid",web.reqString("uid")));

    Gson g = new Gson();
    out.write( g.toJson(files) );
%>