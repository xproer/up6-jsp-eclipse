<%@ page language="java" import="net.sf.json.JSONObject" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.sql.DataBaseType" %>
<%@	page import="up6.sql.SqlSort" %>
<%@ page import="up6.sql.SqlTable" %>
<%@ page import="up6.sql.SqlWhere" %>
<%@ page import="up6.utils.ConfigReader" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.util.List" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="up6.utils.JsonDateValueProcessor" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);
    String pid = web.queryString("pid");
    String uid = web.reqString("uid");
    String pathRel = web.reqStringDecode("pathRel");
    if (!pathRel.endsWith("/")) pathRel += '/';
    boolean isRootDir = StringUtils.equals(pathRel,"/");
    String key = web.reqStringDecode("key");

    //f_pathRel = 'a' + f_nameLoc
    String pathRelSql = String.format("f_pathRel='%s'+f_nameLoc",pathRel);
    if(ConfigReader.dbType() != DataBaseType.SqlServer)
        pathRelSql = String.format("f_pathRel=CONCAT('%s',f_nameLoc)",pathRel);
    if(isRootDir) pathRelSql="";
    //从文件表中搜索
    List<FileInf> files = SqlTable.build("up6_files").reads(FileInf.build(),
            "f_id,f_pid,f_nameLoc,f_sizeLoc,f_lenLoc,f_time,f_pidRoot,f_fdTask,f_pathSvr,f_pathRel,f_lenLocSec,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec,f_object_key",
            SqlWhere.build()
                    .sql("f_pathRel",pathRelSql)
                    .sql("key",String.format("f_nameLoc like '%%%s%%'",key))
                    .eq("f_uid",uid)
                    .eq("f_complete",true)
                    .eq("f_deleted",false),
            SqlSort.build().desc("f_fdTask").desc("f_time")
    );

    //从目录表中搜索
    List<FileInf> fds = SqlTable.build("up6_folders").reads(FileInf.build(),
            "f_id,f_nameLoc,f_pid,f_sizeLoc,f_time,f_pidRoot,f_pathRel",
            SqlWhere.build()
                    .sql("f_pathRel",pathRelSql)
                    .sql("key",String.format("f_nameLoc like '%%%s%%'",key))
                    .eq("f_uid",uid)
                    .eq("f_complete",true)
                    .eq("f_deleted",false),
            SqlSort.build().desc("f_time")
    );

    for (FileInf f : fds)
    {
        f.fdTask=true;
        files.add(0,f);
    }

    JSONObject o = new JSONObject();
    o.put("count", files.size());
    o.put("code", 0);
    o.put("msg", "");
    o.put("data", JSONArray.fromObject(files.toArray(), JsonDateValueProcessor.build()));

    System.out.println(o.toString());
    out.write(o.toString());
%>