<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.model.FileInf" %>
<%@	page import="up6.sql.SqlTable" %>
<%@ page import="up6.sql.SqlWhere" %>
<%@ page import="java.util.List" %>
<%
	/**
	 * 加载未上传完的文件列表
	 */
    out.clear();

    List<FileInf> files = SqlTable.build("up6_files").reads(FileInf.build(),
            SqlWhere.build()
                    .eq("f_complete",false)
                    .eq("f_fdChild",false)
                    .eq("f_deleted",false)
    );
    Gson g = new Gson();
    out.write( g.toJson(files) );
%>