<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.FolderBuilder" %>
<%@ page import="up6.utils.WebBase" %>
<%@	page import="java.io.UnsupportedEncodingException" %>
<%@ page import="java.net.URLEncoder" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);


    String id		= web.queryString("id");
    String callback = web.queryString("callback");

    if (StringUtils.isBlank(id))
    {
        web.toContent(callback + "({\"value\":null})");
        return;
    }
    FolderBuilder fb = new FolderBuilder();
    Gson gson = new Gson();
    String json = gson.toJson(fb.build(id));

    try {
        json = URLEncoder.encode(json,"UTF-8");
    } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }//编码，防止中文乱码
    json = json.replace("+","%20");
    json = callback + "({\"value\":\"" + json + "\"})";//返回jsonp格式数据。
    out.write(json);
%>