<%@ page language="java" import="net.sf.json.JSONArray" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.sql.SqlExec" %>
<%@	page import="up6.sql.SqlParam" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String pid = web.reqString("pid");
    SqlWhereMerge swm = new SqlWhereMerge();
    swm.equal("f_fdChild", 0);
    swm.equal("f_fdTask", 1);
    swm.equal("f_deleted", 0);
    if (!StringUtils.isBlank(pid)) swm.equal("f_pid", pid);

    SqlExec se = new SqlExec();
    JSONArray arr = new JSONArray();
    JSONArray data = se.select("up6_files"
            , "f_id,f_pid,f_pidRoot,f_nameLoc"
            , swm.to_sql()
            ,"");

    //查子目录
    if (!StringUtils.isBlank(pid))
    {
        data = se.select("up6_folders"
                , "f_id,f_pid,f_pidRoot,f_nameLoc"
                , new SqlParam[] {
                        new SqlParam("f_pid", pid)
                        ,new SqlParam("f_deleted", false)
                },"");
    }

    for(int i = 0 , l = data.size() ; i<l;++i)
    {
        JSONObject item = new JSONObject();
        JSONObject f = (JSONObject)data.get(i);
        item.put("id", f.getString("f_id") );
        item.put("text", f.getString("f_nameLoc"));
        item.put("parent", "#");
        item.put("nodeSvr", f);
        arr.add(item);
    }
    out.write( arr.toString() );

%>