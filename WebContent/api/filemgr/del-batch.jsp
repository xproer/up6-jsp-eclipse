<%@ page language="java" import="net.sf.json.JSONArray" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="up6.utils.PathTool" %>
<%@	page import="up6.utils.WebBase" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);


    String data = web.reqString("data");
    data = PathTool.url_decode(data);
    JSONArray o = JSONArray.fromObject(data);

    SqlExec se = new SqlExec();
    se.exec_batch("up6_files", "update up6_files set f_deleted=1 where f_id=?", "", "f_id", o);
    se.exec_batch("up6_folders", "update up6_folders set f_deleted=1 where f_id=?", "", "f_id", o);

    JSONObject ret = new JSONObject();
    ret.put("ret", 1);
    out.write(ret.toString());
%>