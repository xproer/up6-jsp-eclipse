<%@ page language="java" import="net.sf.json.JSONObject" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.sql.SqlExec" %>
<%@	page import="up6.sql.SqlParam" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.utils.WebBase" %>
<%
    out.clear();
    WebBase web = new WebBase(pageContext);
    String id = web.queryString("id");
    String pathRel = web.reqStringDecode("pathRel");
    pathRel += '/';

    SqlWhereMerge swm = new SqlWhereMerge();
    DBConfig cfg = new DBConfig();
    if(StringUtils.equals(cfg.m_db, "sql"))
    {
        swm.charindex(pathRel,"f_pathRel");
    }
    else
    {
        swm.instr(pathRel,"f_pathRel");
    }
    String where = swm.to_sql();

    SqlExec se = new SqlExec();
    se.update("up6_files", new SqlParam[] {new SqlParam("f_deleted",true)}, where);
    se.update("up6_files"
            , new SqlParam[] {new SqlParam("f_deleted",true)}
            , new SqlParam[] {new SqlParam("f_id",id)}
    );
    se.update("up6_folders", new SqlParam[] {new SqlParam("f_deleted",true)}, where);
    se.update("up6_folders"
            , new SqlParam[] {new SqlParam("f_deleted",true)}
            , new SqlParam[] {new SqlParam("f_id",id)}
    );

    JSONObject ret = new JSONObject();
    ret.put("ret", 1);
    out.write(ret.toString());
%>