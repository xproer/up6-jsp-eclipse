<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.biz.*" %>
<%@ page import="up6.database.*" %>
<%@ page import="up6.store.*" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%
out.clear();
/*
	此页面主要用来向数据库添加一条记录。
	一般在 HttpUploader.js HttpUploader_MD5_Complete(obj) 中调用
	更新记录：
		2012-05-24 完善
		2012-06-29 增加创建文件逻辑，
*/

WebBase web = new WebBase(pageContext);
String md5 		= web.queryString("md5");
String uid 		= web.reqString("uid");
String id		= web.queryString("id");
String pid 		= web.queryString("pid");
String callback = web.queryString("callback");//jsonp
int cover		= web.reqInt("cover");//是否覆盖
String nameLoc	= web.queryString("nameLoc");//文件名称
nameLoc			= PathTool.url_decode(nameLoc);

//返回值。1表示成功
int ret = 0;
if ( !StringUtils.isBlank(id))
{
	DBFile.build().complete(id);
	
	up6_biz_event.file_post_complete(id);
	ret = 1;
}
%><%=callback + "(" + ret + ")"%>