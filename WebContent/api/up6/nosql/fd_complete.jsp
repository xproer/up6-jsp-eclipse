<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="up6.biz.FolderSchemaDB" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="up6.model.FileInf" %>
<%
out.clear();
/*
	此页面主要更新文件夹数据表。已上传字段
	更新记录：
		2022-05-12 层级结构信息从文件中解析
		2019-05-29 增加对子目录上传处理
		2014-07-23 创建
*/

WebBase web = new WebBase(pageContext);
String id		= web.queryString("id");
String uid 		= web.queryString("uid");
String cbk 		= web.queryString("callback");//jsonp
int ret = 0;

//参数为空
if (	!StringUtils.isBlank(uid)||
		!StringUtils.isBlank(id))
{
	
	up6_biz_event.folder_post_complete(id);
	
	ret = 1;
}
out.write(cbk + "(" + ret + ")");
%>