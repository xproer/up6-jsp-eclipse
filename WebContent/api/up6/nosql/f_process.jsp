<%@ page language="java" import="up6.database.DBFile" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.biz.*" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
out.clear();
/*
	更新文件进度或文件夹进度，百分比
*/

String id 			= request.getParameter("id");
String uid 			= request.getParameter("uid");
String offset 		= request.getParameter("offset");
String lenSvr 		= request.getParameter("lenSvr");
String perSvr 		= request.getParameter("perSvr");
String callback 	= request.getParameter("callback");
int ret = 0;

if (	!StringUtils.isBlank(id)&&
		!StringUtils.isBlank(lenSvr)&&
		!StringUtils.isBlank(perSvr))
	{
		up6_biz_event.file_post_process(id);
		ret = 1;
	}
%><%=callback + "({\"value\":"+ret+"})"%>