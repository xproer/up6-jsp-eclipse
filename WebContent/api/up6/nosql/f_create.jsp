<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.PathBuilderUuid" %>
<%@ page import="up6.biz.WebSafe" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@	page import="up6.model.FileInf" %>
<%@	page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.utils.ConfigReader" %>
<%@ page import="up6.utils.CryptoTool" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.net.URLEncoder" %>
<%
	out.clear();
	/*
		所有单个文件均以md5模式存储。
		更新记录：
			2012-05-24 完善
			2012-06-29 增加创建文件逻辑，
			2015-07-30 取消文件夹层级结构存储规则，改为使用日期存储规则，文件夹层级结构仅保存在数据库中。
			2016-01-07 返回值改为JSON
			2016-04-09 完善逻辑。
			2017-07-13 取消生成id操作
	*/
	WebBase web = new WebBase(pageContext);
	String id        = web.queryString("id");
	String md5       = web.queryString("md5");
	String uid       = web.queryString("uid");
	String lenLoc    = web.queryString("lenLoc");     //数字化的文件大小。12021
	String sizeLoc   = web.queryString("sizeLoc");    //格式化的文件大小。10MB
	String blockSize = web.queryString("blockSize","5242880");  //块大小
	String token     = web.queryString("token");
	String callback  = web.queryString("callback");
	String pathLoc   = web.reqStringDecode("pathLoc");

	//参数为空
	if (	StringUtils.isBlank(md5)
		&& StringUtils.isBlank(uid)
		&& StringUtils.isBlank(sizeLoc))
	{
		out.write(callback + "({\"value\":null})");
		return;
	}

	FileInf fileSvr= new FileInf();
	fileSvr.id      = id;
	fileSvr.fdChild = false;
	fileSvr.uid     = uid;
	fileSvr.nameLoc = PathTool.getName(pathLoc);
	fileSvr.nameSvr = fileSvr.nameLoc;
	fileSvr.pathLoc = pathLoc;
	fileSvr.lenLoc  = Long.parseLong(lenLoc);
	fileSvr.lenLocSec = web.reqLong("lenLocSec");
	fileSvr.sizeLoc = sizeLoc;
	fileSvr.deleted = false;
	fileSvr.md5     = md5;
	fileSvr.blockSize = Integer.parseInt(blockSize);
	fileSvr.blockSizeSec = web.reqInt("blockSizeSec");
	fileSvr.encrypt   = ConfigReader.storageEncrypt();  //存储加密
	fileSvr.encryptAgo = web.reqString("encryptAgo");

	WebSafe ws = new WebSafe();
	boolean ret = ws.validToken(token, fileSvr);
	if(!ret)
	{
		String m = callback + "({\"value\":\"0\",\"ret\":false,\"error\":\"token error\"})";
		out.write(m);
		return;
	}

	//所有单个文件均以uuid/file方式存储
	PathBuilderUuid pb = new PathBuilderUuid();
	fileSvr.pathSvr = pb.genFile(fileSvr.uid,fileSvr);
	fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");

	FileBlockWriter fw = ConfigReader.blockWriter();
	try {
		fileSvr.object_id = fw.make(fileSvr);
	} catch (IOException e) {
		e.printStackTrace();
		JSONObject obj = new JSONObject();
		obj.put("value", "");
		obj.put("error", e.getMessage());
		obj.put("ret", false);
		out.write(callback + "(" + obj.toString() + ")");
		return;
	}

	//触发事件
	up6_biz_event.file_create(fileSvr);


	//将路径转换成相对路径
	fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);

	Gson gson = new Gson();
	String json = gson.toJson(fileSvr);

	json = URLEncoder.encode(json,"UTF-8");//编码，防止中文乱码
	json = json.replace("+","%20");
	json = callback + "({\"value\":\"" + json + "\",\"ret\":true})";//返回jsonp格式数据。
	out.write(json);
%>