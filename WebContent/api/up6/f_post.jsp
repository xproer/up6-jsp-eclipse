<%@ page language="java" import="net.sf.json.JSONObject" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.PathBuilder" %>
<%@ page import="up6.biz.WebSafe" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.utils.*" %>
<%@ page import="java.io.ByteArrayOutputStream" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Map" %>
<%
	out.clear();
	/*
		此页面负责将文件块数据写入文件中。
		此页面一般由控件负责调用
		参数：
			uid
			idSvr
			md5
			lenSvr
			pathSvr
			RangePos
			fd_idSvr
			fd_lenSvr
		更新记录：
			2022-05-30 完善Minio块合并逻辑
			2017-10-23 增加删除文件块缓存操作
			2017-07-13 取消数据库操作
			2016-04-09 优化文件存储逻辑，增加更新文件夹进度逻辑
			2015-03-19 客户端提供pathSvr，此页面减少一次访问数据库的操作。
			2014-07-23 优化代码。
			2012-10-25 整合更新文件进度信息功能。减少客户端的AJAX调用。
			2012-04-18 取消更新文件上传进度信息逻辑。
			2012-04-12 更新文件大小变量类型，增加对2G以上文件的支持。
	*/
	WebBase wb = new WebBase(pageContext);
	String uid 			= request.getHeader("uid");//
	String id 			= request.getHeader("id");
	String pid          = request.getHeader("pid");
	String pidRoot      = request.getHeader("pidRoot");
	String lenSvr		= request.getHeader("lenSvr");
	String lenLoc		= request.getHeader("lenLoc");//本地文件大小
	String encryptAgo 	= request.getHeader("encryptAgo");//加密算法，aes,sm4
	Long lenLocSec 		= wb.headLong("lenLocSec");//文件加密大小
	String blockOffset	= request.getHeader("blockOffset");
	int blockSize		= request.getIntHeader("blockSize");//原始块大小
	boolean blockEncrypt = StringUtils.equalsIgnoreCase(request.getHeader("blockEncrypt"),"true");//是否加密
	int blockIndex		= request.getIntHeader("blockIndex");//块索引，基于1
	int blockCount   	= request.getIntHeader("blockCount");//块总数
	String blockMd5		= request.getHeader("blockMd5");
	boolean complete	= StringUtils.equalsIgnoreCase(request.getHeader("complete"),"true");
	String object_id   	= request.getHeader("object_id");//
	String pathSvr		= "";
	String pathLoc		= "";
	String pathRel		= "";//相对路径
	String token		= request.getHeader("token");
	boolean isLastBlock = complete;

	//参数为空
	if(	 StringUtils.isEmpty( uid )||
			StringUtils.isBlank( id )||
			StringUtils.isEmpty( blockOffset ))
	{
		JSONObject o = new JSONObject();
		o.put("blockOffset",blockOffset);
		o.put("id",id);
		wb.send_erro("paramEmpty",o);

		return;
	}

	HttpRequest form = new HttpRequest(pageContext);
	FileItem blockData = form.getFile("file");//文件块
	pathSvr = form.getBase64("pathSvr");
	pathRel = form.getBase64("pathRel");
	pathLoc = form.getBase64("pathLoc");

	ByteArrayOutputStream ostm = StreamTool.toStream(blockData);
	if(StringUtils.isBlank( pathLoc )) pathLoc = new String(blockData.getName().getBytes(),"UTF-8");

	FileInf fileSvr = new FileInf();
	fileSvr.id = id;
	fileSvr.pid = pid;
	fileSvr.pidRoot = pidRoot;
	fileSvr.object_id = object_id;
	fileSvr.lenSvr = Long.parseLong(lenSvr);
	fileSvr.lenLoc = Long.parseLong(lenLoc);
	fileSvr.lenLocSec = lenLocSec;
	fileSvr.sizeLoc = PathTool.BytesToString(fileSvr.lenLoc);
	fileSvr.pathLoc = pathLoc;
	fileSvr.pathSvr = pathSvr;
	fileSvr.pathRel = pathRel;
	fileSvr.perSvr="100%";
	fileSvr.complete = complete;
	fileSvr.blockIndex = blockIndex;
	fileSvr.blockOffset = Long.parseLong(blockOffset);
	fileSvr.blockOffsetCry = wb.headLong("blockOffsetCry");
	fileSvr.blockSize = blockSize;//原始块大小
	fileSvr.blockSizeSec = wb.headInt("blockSizeCry");
	fileSvr.blockSizeCpr = wb.headInt("blockSizeCpr");
	fileSvr.blockCprType = wb.headStr("blockCprType");
	fileSvr.blockCount = blockCount;
	fileSvr.blockEncrypt = StringUtils.equalsIgnoreCase(request.getHeader("blockEncrypt"),"true");
	fileSvr.encryptAgo = wb.headStr("blockEncryptAgo");
	fileSvr.nameLoc = PathTool.getName(pathLoc);
	fileSvr.nameSvr = fileSvr.nameLoc;
	fileSvr.encrypt= ConfigReader.storageEncrypt();//存储加密

	//验证文件块MD5
	if(!UtilsTool.check_md5(ostm,blockMd5))
	{
		String md5Svr = Md5Tool.fileToMD5(ostm);
		JSONObject o = new JSONObject();
		o.put("fileSvr",JSONObject.fromObject(fileSvr));
		o.put("md5Svr",md5Svr);
		o.put("md5Loc",blockMd5);
		wb.send_erro("blockMd5ValidFail",o);
		return;
	}

	//解压缩
	ostm = UtilsTool.unCompress(ostm,fileSvr);

	//解密
	ostm = UtilsTool.decrypt(ostm,fileSvr);

	//token验证
	WebSafe ws = new WebSafe();
	if(!ws.validToken(token,fileSvr,"block"))
	{
		JSONObject o = new JSONObject();
		o.put("fileSvr",JSONObject.fromObject(fileSvr));
		o.put("token",token);
		wb.send_erro("tokenValidFail",o);
		return;
	}

	PathBuilder pb = new PathBuilder();
	fileSvr.pathSvr = pb.relToAbs(fileSvr.pathSvr);

	//块存储器
	FileBlockWriter fw = ConfigReader.blockWriter();
	boolean needGenId = !StringUtils.isEmpty(pid);//仅子文件创建
	if(needGenId) needGenId = 1 == fileSvr.blockIndex;
	if(needGenId &&
			(fw.storage == StorageType.FastDFS||
			fw.storage == StorageType.Minio||
			fw.storage == StorageType.OSS||
			fw.storage == StorageType.OBS))
		needGenId = StringUtils.isEmpty(fileSvr.object_id);

	//子文件.仅第一块生成存储.id
	try {
		if(needGenId) {
			fileSvr.object_id = fw.make(fileSvr);
			fileSvr.object_key = fileSvr.ObjectKey();
		}
		//保存到层级信息
		fileSvr.saveScheme();
		//保存文件块数据
		fw.write(fileSvr,ostm);
		//合并文件
		if(isLastBlock) fw.writeLastPart(fileSvr);
	}
	catch (IOException e)
	{
		e.printStackTrace();

		JSONObject o = new JSONObject();
		o.put("fileSvr",JSONObject.fromObject(fileSvr));
		o.put("exception",e.getMessage());
		wb.send_erro("writeBlockError",o);
		return;
	}

	up6_biz_event.file_post_block(id,blockIndex);

	JSONObject o = new JSONObject();
	o.put("msg", "ok");
	o.put("offset", blockOffset);
	JSONObject fds = new JSONObject();
	fds.put("object_id",fileSvr.object_id);//将UploadId回传给控件
	if(1 == fileSvr.blockIndex) o.put("fields",fds);

	blockData.delete();
	out.write(o.toString());
%>