<%@ page language="java" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="up6.biz.FolderSchema" %>
<%@ page import="up6.biz.PathBuilderUuid" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.model.FileInf" %>
<%@ page import="up6.utils.PathTool" %>
<%@ page import="up6.utils.WebBase" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%
	out.clear();
	/*
		更新记录：
			2014-07-23 创建
			2014-08-05 修复BUG，上传文件夹如果没有子文件夹时报错的问题。
			2015-07-30 将子文件命名方式改为 md5 方式，不再使用原文件名称存储，防止冲突。
			2016-04-09 完善存储逻辑。

		JSON格式化工具：http://tool.oschina.net/codeformat/json
		POST数据过大导致接收到的参数为空解决方法：http://sishuok.com/forum/posts/list/2048.html
	*/
	WebBase web     = new WebBase(pageContext);
	String id       = web.queryString("id");
	String uid      = web.reqString("uid");
	String lenLoc   = web.queryString("lenLoc");
	String sizeLoc  = web.queryString("sizeLoc");
	String pathLoc  = web.queryString("pathLoc");
	pathLoc         = URLDecoder.decode(pathLoc,"UTF-8");//utf-8解码
	String callback = web.queryString("callback");//jsonp


	//参数为空
	if (StringUtils.isBlank(id)||
			StringUtils.isBlank(pathLoc))
	{
		out.write(callback + "({\"value\":null})");
		return;
	}

	FileInf fileSvr = new FileInf();
	fileSvr.id      = id;
	fileSvr.fdChild = false;
	fileSvr.fdTask  = true;
	fileSvr.uid     = uid;
	fileSvr.nameLoc = PathTool.getName(pathLoc);
	fileSvr.pathLoc = pathLoc;
	fileSvr.pathRel = "";
	fileSvr.lenLoc  = Long.parseLong(lenLoc);
	fileSvr.sizeLoc = sizeLoc;
	fileSvr.deleted = false;
	fileSvr.nameSvr = fileSvr.nameLoc;

	//生成存储路径
	PathBuilderUuid pb = new PathBuilderUuid();
	fileSvr.pathSvr = pb.genFolder(fileSvr);
	fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");
	if(!PathTool.createDirectory(fileSvr.pathSvr))
	{
		out.clear();
		out.write("create dir error");
		response.setStatus(500);
		return;
	}

	//创建层级结构
	FolderSchema fst = new FolderSchema();
	if(!fst.create(fileSvr))
	{
		out.clear();
		out.write("create dir schema error");
		response.setStatus(500);
		return;
	}

	//添加到数据表
	DBFile.build().Add(fileSvr);

	up6_biz_event.folder_create(fileSvr);

	Gson g = new Gson();
	String json = g.toJson(fileSvr);
	json = URLEncoder.encode(json,"utf-8");
	json = json.replace("+","%20");

	JSONObject ret = new JSONObject();
	ret.put("value",json);
	json = callback + String.format("(%s)",ret.toString());//返回jsonp格式数据。
	out.write(json);
%>