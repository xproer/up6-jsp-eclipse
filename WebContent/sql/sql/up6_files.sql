CREATE TABLE [dbo].[up6_files](
	[f_id]         [char](32) NOT NULL,
	[f_pid]        [char](32) NULL,
	[f_pidRoot]    [char](32) NULL,
	[f_fdTask]     [bit] NULL,
	[f_fdChild]    [bit] NULL,
	[f_uid]        [nvarchar](255) default '0',
	[f_nameLoc]    [nvarchar](255) NULL,
	[f_nameSvr]    [nvarchar](255) NULL,
	[f_pathLoc]    [nvarchar](512) NULL,
	[f_pathSvr]    [nvarchar](512) NULL,
	[f_pathRel]    [nvarchar](512) NULL,
	[f_md5]      [nvarchar](40) NULL,
	[f_lenLoc]     [bigint] NULL,
	[f_lenLocSec] 	[bigint] NULL,
	[f_sizeLoc]    [nvarchar](10) NULL,
	[f_pos]      [bigint] NULL,
	[f_lenSvr]     [bigint] NULL,
	[f_perSvr]     [nvarchar](6) NULL,
	[f_complete]   [bit] NULL,
	[f_time]       [datetime] NULL,
	[f_deleted]    [bit] NULL,
	[f_scan]       [bit] NOT NULL,
	[f_encrypt]    [bit] NULL,
	[f_encryptAgo] [nvarchar](7) default 'aes',/*aes,sm4*/
	[f_blockSize]  [int] NULL,
	[f_blockSizeSec] [int] default 0,
	[f_object_key]  [nvarchar](512) default '',
 CONSTRAINT [PK_up6_files_1] PRIMARY KEY CLUSTERED 
(
	[f_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件唯一GUID,由控件生成' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父级文件夹ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pid'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根级文件夹ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pidRoot'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表示是否是一个文件夹上传任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_fdTask'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是文件夹中的子项' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_fdChild'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在本地电脑中的名称。例：QQ.exe ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_nameLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的名称。一般为文件MD5+扩展名。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_nameSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在本地电脑中的完整路径。
示例：D:\Soft\QQ.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pathLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的完整路径。
示例：F:\ftp\user1\QQ2012.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pathSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的相对路径。
示例：/www/web/upload/QQ2012.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pathRel'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件MD5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_md5'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件总长度。以字节为单位
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_lenLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'格式化的文件尺寸。示例：10MB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_sizeLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件续传位置。
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_pos'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已上传长度。以字节为单位。
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_lenSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已上传百分比。示例：10%' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_perSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已上传完毕。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件上传时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_deleted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已经扫描完毕，提供给大型文件夹使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_scan'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件加密后的大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_lenLocSec'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件是否加密' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_encrypt'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件块加密后的大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up6_files', @level2type=N'COLUMN',@level2name=N'f_blockSize'
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pid]  DEFAULT ('') FOR [f_pid]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pidRoot]  DEFAULT ('') FOR [f_pidRoot]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_fdTask]  DEFAULT ((0)) FOR [f_fdTask]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_fdChild]  DEFAULT ((0)) FOR [f_fdChild]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_uid]  DEFAULT ((0)) FOR [f_uid]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_nameLoc]  DEFAULT ('') FOR [f_nameLoc]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_nameSvr]  DEFAULT ('') FOR [f_nameSvr]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pathLoc]  DEFAULT ('') FOR [f_pathLoc]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pathSvr]  DEFAULT ('') FOR [f_pathSvr]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pathRel]  DEFAULT ('') FOR [f_pathRel]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_md5]  DEFAULT ('') FOR [f_md5]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_lenLoc]  DEFAULT ((0)) FOR [f_lenLoc]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_sizeLoc]  DEFAULT ('0Bytes') FOR [f_sizeLoc]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_pos]  DEFAULT ((0)) FOR [f_pos]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_lenSvr]  DEFAULT ((0)) FOR [f_lenSvr]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_perSvr]  DEFAULT ('0%') FOR [f_perSvr]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_complete]  DEFAULT ((0)) FOR [f_complete]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_time]  DEFAULT (getdate()) FOR [f_time]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_deleted]  DEFAULT ((0)) FOR [f_deleted]
GO

ALTER TABLE [dbo].[up6_files] ADD  CONSTRAINT [DF_up6_files_f_scan]  DEFAULT ((0)) FOR [f_scan]
GO

ALTER TABLE [dbo].[up6_files] ADD  DEFAULT ((0)) FOR [f_lenLocSec]
GO

ALTER TABLE [dbo].[up6_files] ADD  DEFAULT ((0)) FOR [f_encrypt]
GO

ALTER TABLE [dbo].[up6_files] ADD  DEFAULT ((0)) FOR [f_blockSize]
GO


