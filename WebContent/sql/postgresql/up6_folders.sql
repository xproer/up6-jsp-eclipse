CREATE TABLE IF NOT EXISTS up6_folders (
  f_id 				char(32) NOT NULL ,
  f_nameLoc 			varchar(255) default '',
  f_pid 				char(32) default '',
  f_uid 				varchar(255) default '0',
  f_lenLoc 			bigint default 0,
  f_sizeLoc 			varchar(50) default '0',
  f_pathLoc 			varchar(255) default '',
  f_pathSvr 			varchar(255) default '',
  f_pathRel 			varchar(255) default '',
  f_folders 			int default 0,
  f_fileCount 		int default 0,
  f_filesComplete 	int default 0,
  f_complete 			boolean default false,
  f_deleted 			boolean default false,
  f_time 				timestamp NULL default CURRENT_TIMESTAMP,
  f_pidRoot 			char(32) default '',
  PRIMARY KEY  (f_id)
)