/*--drop table down_files*/
CREATE TABLE down_files
(
 f_id      		char(32) NOT NULL     
,f_uid        	varchar(255)DEFAULT '0' 
,f_mac        	varchar(50) DEFAULT '' 
,f_nameLoc		varchar(255)DEFAULT ''
,f_pathLoc      varchar(255)DEFAULT '' 	
,f_fileUrl      varchar(255)DEFAULT '' 	
,f_perLoc    	varchar(6) 	default 0 
,f_lenLoc    	bigint 	default 0 
,f_lenSvr		bigint  default 0
,f_sizeSvr      varchar(10) default '0'
,f_complete		boolean	default false
,f_fdTask		boolean 	default false
,PRIMARY KEY  (f_id)
)
