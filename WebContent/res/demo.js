$(function(){
    var demos=[
        {
            name:"NOSQL：",
            pages:[
                {
                    name:"多标签",
                    url:"view/nosql/index.html"
                },
                {
                    name:"单面板",
                    url:"view/nosql/panel.html"
                }]
        },
        {
            name:"上传：",
            pages:[
                {
                    name:"清空数据库",
                    url:"api/up6/clear.jsp"
                },
                {
                    name:"多标签",
                    url:"index.html"
                },
                {
                    name:"单面板",
                    url:"panel.html"
                },
                {
                    name:"单文件",
                    url:"single.html"
                },
                {
                    name:"vue.js示例",
                    url:"vue.html"
                },
                {
                    name:"layer弹窗",
                    url:"layer.html"
                },
                {
                    name:"文件管理器示例",
                    url:"filemgr.html"
                }
            ]
        },
        {
            name:"加密和压缩：",
            pages:[
                {
                    name:"sm4(国密)加密传输",
                    url:"view/safe/sm4.html"
                },
                {
                    name:"sm4(国密)-gzip-加密压缩传输",
                    url:"view/safe/sm4-gzip.html"
                },
                {
                    name:"aes加密传输",
                    url:"view/safe/aes.html"
                },
                {
                    name:"aes-gzip-md5加密传输",
                    url:"view/safe/aes-gzip-md5.html"
                },
                {
                    name: "aes-md5加密传输",
                    url: "view/safe/aes-md5.html"
                },
                {
                    name:"aes-gzip-加密压缩传输",
                    url:"view/safe/aes-gzip.html"
                },
                {
                    name:"gzip-压缩传输",
                    url:"view/safe/gzip.html"
                },
                {
                    name:"zip-压缩传输",
                    url:"view/safe/zip.html"
                }
            ]
        },
        {
            name:"下载：",
            pages:[
                {
                    name:"清空数据库",
                    url:"api/down2/clear.jsp"
                },
                {
                    name:"下载示例",
                    url:"down2.html"
                },
                {
                    name:"ligerui示例",
                    url:"down2-ligerui.html"
                }
            ]
        }
    ];

    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("res/demo.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("res/demo.js"));
            }
        }
        return jsPath;
    };

    //http://localhost/res/down2/
    var root = this.getJsDir();

    $(demos).each(function(i,n){
        var tmps=["<ul>","<li><b>"+n.name+"</b></li>"];
        $(n.pages).each(function(i,n){
            tmps.push("<li><a target='_blank' href='"+root+n.url+"'>"+n.name+"</a></li>");
        });
        tmps.push("</ul>");
        $("#demos").append(tmps.join(""));
    });
});