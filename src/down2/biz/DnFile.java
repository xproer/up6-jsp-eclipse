package down2.biz;

import com.google.gson.Gson;
import net.sf.json.JSONObject;
import up6.model.DnFileInf;
import up6.sql.SqlSeter;
import up6.sql.SqlTable;
import up6.sql.SqlWhere;
import up6.utils.PathTool;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class DnFile 
{
	public static DnFile build(){return new DnFile();}

	public DnFile()
	{
	}
	
    public void Add(DnFileInf inf) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").insert(inf);
    }

    /**
     * 将文件设为已完成
     * @param fid
     */
    public void Complete(String fid) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").update(
    			SqlSeter.build().set("f_complete",true),
				SqlWhere.build().eq("f_id",fid)
		);
    }

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="fid"></param>
    public void Delete(String fid,String uid) throws SQLException, ParseException {
    	SqlTable.build("down_files").del(
    			SqlWhere.build().eq("f_id",fid)
				.eq("f_uid",uid)
		);
    }
    
    /**
     * 更新文件进度信息
     * @param fid
     * @param uid
     * @param lenLoc
     */
    public void process(String fid,String uid,String lenLoc,String perLoc) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").update(
    			SqlSeter.build().set("f_lenLoc",Long.parseLong(lenLoc))
				.set("f_perLoc",perLoc),
				SqlWhere.build().eq("f_id",fid).eq("f_uid",uid)
		);
    }

    /// <summary>
    /// 获取所有未下载完的文件列表
    /// </summary>
    /// <returns></returns>
    public static String all_uncmp(String uid) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
    	List<DnFileInf> fs = SqlTable.build("down_files").reads(DnFileInf.build(),
				SqlWhere.build().eq("f_uid",uid));

        Gson g = new Gson();
	    return g.toJson( fs );
	}
    
    /**
     * 从up6_files表中获取已经上传完的数据
     * @param uid
     * @return
     */
    public String all_complete(String uid) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
		List<DnFileInf> fs = SqlTable.build("up6_files").reads(DnFileInf.build(),
				"f_id,f_fdTask,f_nameLoc,f_sizeLoc,f_lenSvr,f_pathSvr,f_lenLocSec,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec,f_object_key",
				SqlWhere.build()
				.eq("f_uid",uid)
				.eq("f_deleted",false)
				.eq("f_complete",true)
				.eq("f_fdChild",false)
				.eq("f_scan",true)
		);

		for (DnFileInf f : fs)
		{
			f.f_id = f.id;
			f.sizeSvr = f.sizeLoc;
			f.lenSvrSec = f.lenLocSec;
			f.fields.put("object_key",PathTool.url_safe_encode(f.object_key));
		}
        Gson g = new Gson();
	    return g.toJson( fs );
    }
    
    public void Clear() throws SQLException 
    {
    	SqlTable.build("down_files").clear();
    }

	/**
	 * 获取文件夹所有子文件
	 * @param pidRoot
	 * @return
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws ParseException
	 * @throws IllegalAccessException
	 */
	public String childs(String pidRoot) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
		List<DnFileInf> files = SqlTable.build("up6_files").reads(DnFileInf.build(),
				SqlWhere.build().eq("f_pidRoot",pidRoot)
		);
		for (DnFileInf f : files)
		{
			f.f_id = f.id;
			f.lenSvrSec = f.lenLocSec;
			f.fields.put("object_key",PathTool.url_safe_encode(f.object_key));
		}
		Gson g = new Gson();
		return g.toJson(files);
	}
}