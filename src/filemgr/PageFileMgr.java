package filemgr;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import up6.database.DBFile;
import up6.database.DbFolder;
import up6.model.FileInf;
import up6.sql.*;
import up6.utils.PathTool;

import java.sql.SQLException;
import java.text.ParseException;

/**
 * 文件管理器页面逻辑
 * @author zysoft
 * 更新记录：
 *  2022-12-11 完善重命名目录逻辑
 *
 */
public class PageFileMgr {
	public PageFileMgr(){
	}

	public JSONObject rename_file(String id,String pathRel,String name) throws ParseException, IllegalAccessException, SQLException {
		//新的相对路径
        pathRel = PathTool.getParent(pathRel);
        pathRel = PathTool.combin(pathRel, name);
		
        //根据相对路径(新)=>查找同名文件
		FileInf s = SqlTable.build("up6_files").readOne(FileInf.build(),
                SqlWhere.build()
                        .eq("f_pathRel",pathRel)
                        .eq("f_deleted",false));

        boolean exist = s != null;
		
        //不存在同名文件
        if (!exist)
        {
            SqlTable.build("up6_files").update(
                    SqlSeter.build().set("f_nameLoc",name)
                    .set("f_nameSvr",name)
                    .set("f_pathRel",pathRel),
                    SqlWhere.build().eq("f_id",id)
            );

        	JSONObject v = new JSONObject();
        	v.put("state", true);
        	v.put("pathRel",pathRel);
			return v;
        }
        else
        {
        	JSONObject v = new JSONObject();
        	v.put("state", false);
        	v.put("msg", "存在同名文件");
        	v.put("code", "102");
            //var v = new JObject { { "state",false},{ "msg","存在同名文件"},{ "code","102"} };
            return v;
        }		
	}

    /**
     * 重命名根目录名称
     * @param id
     * @param pid
     * @param name
     */
	public JSONObject renameRootDir(String id,String pathRel,String name) throws ParseException, IllegalAccessException, SQLException {
        String pathRelOld = pathRel;
		//新的相对路径
	    String pathRelNew = "/" + name;
        FileInf s = SqlTable.build("up6_files").readOne(FileInf.build(),
                SqlWhere.build()
                        .eq("f_pathRel",pathRelNew)
                        .eq("f_deleted",false));

        boolean exist = s != null;

        //不存在同名目录
        if(!exist)
        {
            //更新文件夹相对路径，名称
            SqlTable.build("up6_files").update(
                    SqlSeter.build()
                    .set("f_nameLoc",name)
                    .set("f_nameSvr",name)
                    .set("f_pathRel",pathRelNew),
                    SqlWhere.build().eq("f_id",id)
            );

            DBFile.build().updatePathRel(pathRelOld, pathRelNew);
            DbFolder.build().updatePathRel(pathRelOld, pathRelNew);

            JSONObject v = new JSONObject();
            v.put("state", true);
            v.put("pathRel",pathRelNew);
            return v;
        }//存在同名目录
        else
        {
            JSONObject res = new JSONObject();
            res.put("state", false);
            res.put("msg", "存在同名项");
            return  res;
        }
    }

    /**
     * 重命名子目录
     * @param id
     * @param pid
     * @param name
     */
	public JSONObject renameChildDir(String id,String pathRel,String name) throws ParseException, IllegalAccessException, SQLException {
		String pathRelOld = pathRel;
        //root/dir/name => root/dir
        Integer index = pathRel.lastIndexOf('/');
        pathRel = pathRel.substring(0, index + 1);
        //root/dir/old => root/dir/new
        pathRel += name;
        String pathRelNew = pathRel;
		
        FileInf fd = SqlTable.build("up6_folders").readOne(FileInf.build(),
                "f_pathRel",
                SqlWhere.build()
                        .eq("f_pathRel",pathRelNew)
                        .eq("f_deleted",false)
        );

        boolean exist = fd != null;
        //同名目录不存在=>可以重命名
        if(!exist)
        {
            //更新当前目录相对路径
            SqlTable.build("up6_folders").update(
                    SqlSeter.build()
                    .set("f_nameLoc",name)
                    .set("f_pathRel",pathRelNew),
                    SqlWhere.build().eq("f_id",id)
            );

            //更新子文件和子目录相对路径
            DBFile.build().updatePathRel(pathRelOld, pathRelNew);
            DbFolder.build().updatePathRel(pathRelOld, pathRelNew);

            JSONObject v = new JSONObject();
            v.put("state", true);
            v.put("pathRel",pathRelNew);
            return v;

        }//存在同名目录
        else{
            JSONObject v = new JSONObject();
            v.put("state", false);
            v.put("msg", "存在同名目录");
            v.put("code", "102");
            return v;
        }
	}

	public JSONObject rename_folder(String id,String pathRel,String name) throws ParseException, IllegalAccessException, SQLException {
        
		String parentDir = PathTool.getParent(pathRel);
		parentDir = parentDir.replace('\\', '/');
		//根目录
		boolean isRootDir = StringUtils.equals(parentDir,"/");
        if(isRootDir) return this.renameRootDir(id,pathRel,name);
        else return  this.renameChildDir(id,pathRel,name);
	}
}