package up6.database;

import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import up6.model.FileInf;
import up6.sql.SqlExec;
import up6.sql.SqlParam;
import up6.sql.SqlWhereMerge;

public class DbFolderOracle extends DbFolder
{	
	public boolean existSameFolder(String name,String pid) throws SQLException
	{
		SqlExec se = new SqlExec();
		
		//子目录
		if(!StringUtils.isEmpty(pid))
		{
			return se.count("up6_folders",new SqlParam[] {
					new SqlParam("f_nameLoc",name),
					new SqlParam("f_pid",pid),
					new SqlParam("f_deleted",false)
			})>0;
		}//根目录
		else
		{
			SqlWhereMerge swm = new SqlWhereMerge();
	        swm.equal("f_nameLoc", name);
	        swm.equal("f_deleted", 0);
	        if(StringUtils.isBlank(pid)) pid=" ";
	        swm.equal("nvl(f_pid,' ')", pid);
	        String where = swm.to_sql();

	        String sql = String.format("select f_id from up6_files where %s", where);

	        JSONArray fid = se.exec("up6_files", sql, "f_id", "");
	        return fid.size() > 0;			
		}
	}
	
	/**
	 * 更新子文件路径
	 * @param pathRelOld
	 * @param pathRelNew
	 */
	public void updatePathRel(String pathRelOld,String pathRelNew)
	{
        //更新子文件路径
        String sql = String.format("update up6_folders set f_pathRel=REPLACE(f_pathRel,'%s/','%s/') where instr(f_pathRel,'%s/')=1",
            pathRelOld,
            pathRelNew,
            pathRelOld
            );
        
        SqlExec se = new SqlExec();
        se.exec(sql);		
	}
}
