package up6.database;

import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import com.google.gson.GsonBuilder;

import up6.utils.ConfigReader;
import up6.utils.Up6Exception;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据库配置类
 * 
 * @author Administrator
 *
 */
public class DBConfig {
	public String m_db="oracle";//sql,oracle,mysql
	
	String driver = "";
	String url = "";
	String name = "";
	String pass = "";
	
	public DBConfig() {

		ConfigReader cr = new ConfigReader();
		this.m_db = cr.readString("$.database.connection.type");
		JSONObject o = cr.read("$.database.connection."+this.m_db);
		this.driver = o.getString("driver");
		this.url = o.getString("url");
		this.name = o.getString("name");
		this.pass = o.getString("pass");
	}

	public Connection getCon() throws SQLException  
	{
		Connection con = null;
		
		try {
            Class.forName(this.driver).newInstance();
            con = DriverManager.getConnection(this.url,this.name,this.pass);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }//加载驱动。
		
		
		HashMap<String, String> msg = new HashMap<String,String>();
        msg.put("msg", "数据库连接失败，检查是否开启了远程连接");
        msg.put("driver", this.driver);
        msg.put("url", this.url);
        msg.put("pass", this.pass);
        msg.put("pass", this.pass);
		if(null==con) throw new SQLException(new GsonBuilder().disableHtmlEscaping().create().toJson(msg));
		return con;		
	}
}
