package up6.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBFileSQL extends DBFile
{
	public void delFolder(String id,String uid) throws SQLException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("update up6_files set f_deleted=1 where f_id=? and f_uid=?;");
		sb.append("update up6_files set f_deleted=1 where f_pidRoot=? and f_uid=?;");
		sb.append("update up6_folders set f_deleted=1 where f_id=? and f_uid=?;");
		DbHelper db = new DbHelper();
		PreparedStatement cmd = db.GetCommand(sb.toString());
		cmd.setString(1, id);
		cmd.setString(2, uid);
		cmd.setString(3, id);
		cmd.setString(4, uid);
		cmd.setString(5, id);
		cmd.setString(6, uid);
		db.ExecuteNonQuery(cmd);
	}
}