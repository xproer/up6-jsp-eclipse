package up6.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBFileOdbc extends DBFile{

	public void delFolder(String id,String uid) throws SQLException
	{
		DbHelper db = new DbHelper();
		Connection con = db.GetCon();
		
		try {
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();
			stmt.addBatch("update up6_files set f_deleted=True where f_id='" + id + "' and f_uid=" + uid);
			stmt.addBatch("update up6_files set f_deleted=True where f_pidRoot='" + id + "' and f_uid=" + uid);
			stmt.addBatch("update up6_folders set f_deleted=True where f_id='" + id + "' and f_uid=" + uid);
			stmt.executeBatch();
			con.commit();
			stmt.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
