package up6.database;

import up6.sql.SqlExec;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBFileMySQL extends DBFile 
{
	public void delFolder(String id,String uid) throws SQLException
	{
		DbHelper db = new DbHelper();
		Connection con = db.GetCon();
		
		try {
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();
			stmt.addBatch("update up6_files set f_deleted=1 where f_id='" + id + "' and f_uid=" + uid);
			stmt.addBatch("update up6_files set f_deleted=1 where f_pidRoot='" + id + "' and f_uid=" + uid);
			stmt.addBatch("update up6_folders set f_deleted=1 where f_id='" + id + "' and f_uid=" + uid);
			stmt.executeBatch();
			con.commit();
			stmt.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * 更新子文件路径
	 * @param pathRelOld
	 * @param pathRelNew
	 */
	public void updatePathRel(String pathRelOld,String pathRelNew)
	{
        //更新子文件路径
        String sql = String.format("update up6_files set f_pathRel=REPLACE(f_pathRel,'%s/','%s/') where locate('%s/',f_pathRel)=1",
            pathRelOld,
            pathRelNew,
            pathRelOld
            );
        
        SqlExec se = new SqlExec();
        se.exec(sql);
		
	}
}