package up6.database;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;
import up6.sql.SqlExec;
import up6.sql.SqlWhereMerge;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBFileOracle extends DBFile
{
	public void delFolder(String id,String uid) throws SQLException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("begin ");
		sb.append("update up6_files set f_deleted=1 where f_id=? and f_uid=?;");
		sb.append("update up6_files set f_deleted=1 where f_pidRoot=? and f_uid=?;");
		sb.append("update up6_folders set f_deleted=1 where f_id=? and f_uid=?;");
		sb.append(" end;");		
		DbHelper db = new DbHelper();
		PreparedStatement cmd = db.GetCommand(sb.toString());
		cmd.setString(1, id);
		cmd.setString(2, uid);
		cmd.setString(3, id);
		cmd.setString(4, uid);
		cmd.setString(5, id);
		cmd.setString(6, uid);
		db.ExecuteNonQuery(cmd);
	}
	
	public boolean existSameFile(String name,String pid) throws SQLException
	{
		SqlWhereMerge swm = new SqlWhereMerge();
        swm.equal("f_nameLoc", name.trim());
        swm.equal("f_deleted", 0);
        if(StringUtils.isBlank(pid)) pid=" ";
        swm.equal("nvl(f_pid,' ')", pid);
        String where = swm.to_sql();

        String sql = String.format("select f_id from up6_files where %s", where);

        SqlExec se = new SqlExec();
        JSONArray fid = se.exec("up6_files", sql, "f_id", "");
        return fid.size() > 0;		
	}
	
	/**
	 * 更新子文件路径
	 * @param pathRelOld
	 * @param pathRelNew
	 */
	public void updatePathRel(String pathRelOld,String pathRelNew)
	{
        //更新子文件路径
        String sql = String.format("update up6_files set f_pathRel=REPLACE(f_pathRel,'%s/','%s/') where instr(f_pathRel,'%s/')=1",
            pathRelOld,
            pathRelNew,
            pathRelOld
            );
        
        SqlExec se = new SqlExec();
        se.exec(sql);
		
	}
}
