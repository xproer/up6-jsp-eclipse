package up6.database;
import com.google.gson.Gson;
import up6.model.FileInf;
import up6.sql.*;
import up6.utils.ConfigReader;

import java.sql.*;
import java.text.ParseException;
import java.util.List;

/*
 * 原型
*/
public class DBFile {

	public static DBFile build(){
		DataBaseType dt = ConfigReader.dbType();
		if( dt == DataBaseType.SqlServer ) return new DBFileSQL();
		if( dt == DataBaseType.MySQL ) return new DBFileMySQL();
		if( dt == DataBaseType.PostgreSQL ) return new DBFilePostgreSQL();
		if( dt == DataBaseType.Oracle||
				dt == DataBaseType.DMDB) return new DBFileOracle();
		if( dt == DataBaseType.KingBase ) return new DBFileOdbc();
		return new DBFile();
	}

	public DBFile()	{	}

	/**
	 * 获取所有未上传完成的文件列表
	 * @param f_uid
	 * @return
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws ParseException
	 * @throws IllegalAccessException
	 */
	public String GetAllUnComplete(String f_uid) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
		List<FileInf> fs = SqlTable.build("up6_files").reads(FileInf.build(),
				SqlWhere.build()
						.eq("f_uid",f_uid)
						.eq("f_deleted", false)
						.eq("f_fdChild", false)
						.eq("f_complete", false)
						.eq("f_scan", false));
		
		Gson g = new Gson();
	    return g.toJson( fs);//bug:arrFiles为空时，此行代码有异常
	}
	
	public FileInf read(String f_id) throws ParseException, IllegalAccessException, SQLException {
		FileInf f = SqlTable.build("up6_files").readOne(FileInf.build(),
				SqlWhere.build().eq("f_id",f_id));

		return f;
	}

	public FileInf read(String pathRel,String id,String uid) throws ParseException, IllegalAccessException, SQLException {
		return SqlTable.build("up6_files").readOne(FileInf.build(),"f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel",
				SqlWhere.build()
						.eq("f_pathRel", pathRel)
						.eq("f_deleted", false)
						.eq("f_uid", uid)
						.ineq("f_id", id)
                );
	}
	
	/// <summary>
	/// 根据文件MD5获取文件信息
	/// 取已上传完的文件
	/// </summary>
	/// <param name="md5"></param>
	/// <param name="inf"></param>
	/// <returns></returns>
	public FileInf exist_file(String md5) throws ParseException, IllegalAccessException, SQLException {
		FileInf f = SqlTable.build("up6_files").readOne(FileInf.build(),
				SqlWhere.build()
						.eq("f_complete",true)
						.eq("f_md5",md5)
		);

		return f;
	}

	/**
	 * 增加一条数据，在f_create中调用。
	 * 文件名称，本地路径，远程路径，相对路径都使用原始字符串。
	 * d:\soft\QQ2012.exe
	 * @param f
	 * @return
	 */
	public void Add(FileInf f) throws ParseException, IllegalAccessException, SQLException {
		SqlTable.build("up6_files").insert(f);
	}

	/**
	 * 清空文件表，文件夹表数据。
	 * @throws SQLException 
	 */
	public void Clear() throws SQLException  
	{
		SqlTable.build("up6_files").clear();
		SqlTable.build("up6_folders").clear();
	}

	/// <summary>
	/// 更新上传进度
	/// </summary>
	///<param name="f_uid">用户ID</param>
	///<param name="f_id">文件ID</param>
	///<param name="f_pos">文件位置，大小可能超过2G，所以需要使用long保存</param>
	///<param name="f_lenSvr">已上传长度，文件大小可能超过2G，所以需要使用long保存</param>
	///<param name="f_perSvr">已上传百分比</param>
	public boolean f_process(String uid,String f_id,long offset,long f_lenSvr,String f_perSvr) throws SQLException
	{
		SqlExec se = new SqlExec();
		se.update("up6_files"
				, new SqlParam[] {
						new SqlParam("f_pos",offset)
						,new SqlParam("f_lenSvr",f_lenSvr)
						,new SqlParam("f_perSvr",f_perSvr)
						}
				, new SqlParam[] {
						new SqlParam("f_uid",uid)
						,new SqlParam("f_id",f_id)
				});

		return true;
	}

	public void complete(String id) throws ParseException, IllegalAccessException, SQLException {
		SqlTable.build("up6_files").update(SqlSeter.build()
				.sql("f_lenSvr=f_lenLoc")
				.set("f_perSvr","100%")
				.set("f_complete",true)
				.set("f_scan",true),
				SqlWhere.build().eq("f_id",id)
		);
	}

	/// <summary>
	/// 删除一条数据，并不真正删除，只更新删除标识。
	/// </summary>
	/// <param name="f_uid"></param>
	/// <param name="f_id"></param>
	public void Delete(String f_uid,String f_id) throws ParseException, IllegalAccessException, SQLException {
		SqlTable.build("up6_files").update(
				SqlSeter.build().set("f_deleted",true),
				SqlWhere.build().eq("f_uid",f_uid)
				.eq("f_id",f_id)
		);
	}
	
	public void delete(String pathRel,String uid,String id) throws ParseException, IllegalAccessException, SQLException {
		SqlTable.build("up6_files").update(
				SqlSeter.build().set("f_deleted",true),
				SqlWhere.build().eq("f_uid",uid)
				.eq("f_pathRel",pathRel)
				.eq("f_fdTask",false)
				.ineq("f_id",id)
		);
	}
	
	/**
	 * 继承类重写
	 * @param id
	 * @param uid
	 * @throws SQLException 
	 */
	public void delFolder(String id,String uid) throws SQLException {}
	
	public boolean existSameFile(String name,String pid) throws SQLException
	{
        SqlWhereMerge swm = new SqlWhereMerge();
        swm.equal("f_nameLoc", name.trim());
        swm.equal("f_pid", pid.trim());
        swm.equal("f_deleted", 0);

        SqlExec se = new SqlExec();
        return se.count("up6_files", swm.to_sql())>0;		
	}
	
	/**
	 * 更新子文件路径
	 * @param pathRelOld
	 * @param pathRelNew
	 */
	public void updatePathRel(String pathRelOld,String pathRelNew)
	{
        //更新子文件路径
        String sql = String.format("update up6_files set f_pathRel=REPLACE(f_pathRel,'%s/','%s/') where CHARINDEX('%s/',f_pathRel)=1",
            pathRelOld,
            pathRelNew,
            pathRelOld
            );
        
        SqlExec se = new SqlExec();
        se.exec(sql);
		
	}

	//批量添加
	public void addBatch(List<FileInf> arr) throws SQLException, ParseException, IllegalAccessException {
		SqlTable.build("up6_files").inserts(arr);
	}

}