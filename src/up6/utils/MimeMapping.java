package up6.utils;

import java.util.HashMap;
import java.util.Map;

public class MimeMapping {
	 private Map<String, String> m_type = new HashMap<String,String>();
	 
	 public MimeMapping() {
		 this.m_type.put("html", "text/html");
		 this.m_type.put("htm", "text/html");
		 this.m_type.put("txt", "text/plain");
		 this.m_type.put("jpg", "image/jpeg");
		 this.m_type.put("gif", "image/gif");
		 this.m_type.put("png", "image/png");
		 this.m_type.put("mpeg", "image/mpeg");
		 this.m_type.put("", "application/octet-stream");
		 this.m_type.put("pdf", "application/pdf");
		 this.m_type.put("doc", "application/msword");
		 this.m_type.put("docx", "application/msword");
	 }
	 
	 public static String getType(String t)
	 {
		 MimeMapping mm = new MimeMapping();
		 t = PathTool.getExtention(t).toLowerCase();
		 if(mm.m_type.containsKey(t))
		 {
			 return mm.m_type.get(t);
		 }
		 return "application/octet-stream";
	 }
}
