package up6.utils;

import org.apache.commons.lang.StringUtils;
import up6.model.FileInf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class UtilsTool {

    /**
     * 解压缩
     * @param d
     * @param f
     * @return
     * @throws Up6Exception
     */
    public static ByteArrayOutputStream unCompress(ByteArrayOutputStream d, FileInf f) throws Up6Exception {
        //未启用压缩
        if(f.blockSizeCpr==0) return d;

        //压缩块与上传块不相同
        if (f.blockSizeCpr != d.size())
            throw new Up6Exception(f,d,"发送的块大小与接收的块大小不同");

        if(StringUtils.equalsIgnoreCase(f.blockCprType,"gzip"))
            return unGzip(d);
        else if (StringUtils.equalsIgnoreCase(f.blockCprType,"zip"))
            return unZip(d);

        throw new Up6Exception("未知的压缩算法:" + f.blockCprType);
    }

    /**
     * 解密块
     * @param d
     * @param f
     * @return
     * @throws Up6Exception
     */
    public static ByteArrayOutputStream decrypt(ByteArrayOutputStream d, FileInf f) throws Up6Exception {
        int blockSize = f.blockSize;
        //开启了加密
        if(f.blockEncrypt) blockSize = f.blockSizeSec;

        //块大小不同
        if(blockSize != d.size())
            throw new Up6Exception(f,d,"发送的块大小与接收的块大小不同");

        //未加密
        if(!f.blockEncrypt) return d;
        //启用存储加密
        if(f.encrypt) return d;

        CryptoTool ct = new CryptoTool();
        return ct.decrypt(f.encryptAgo,d);
    }

    /**
     * MD5难
     * @param d
     * @return
     */
    public static boolean check_md5(ByteArrayOutputStream d,String md5Loc){
        if (StringUtils.isBlank(md5Loc) ) return true;
        String md5Svr = Md5Tool.fileToMD5(d);
        return StringUtils.equalsIgnoreCase(md5Svr,md5Loc);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="s"></param>
    /// <param name="type">gzip,zip</param>
    /// <returns></returns>
    public static ByteArrayOutputStream unGzip(ByteArrayOutputStream s) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(s.toByteArray());
        try
        {
            GZIPInputStream ungzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int n;
            while ((n = ungzip.read(buffer)) >= 0)
            {
                out.write(buffer, 0, n);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return out;
    }

    public static ByteArrayOutputStream unZip(ByteArrayOutputStream s) {
        ByteArrayInputStream bin =new ByteArrayInputStream(s.toByteArray());
        InflaterInputStream iis = new InflaterInputStream(bin);
        ByteArrayOutputStream o = new ByteArrayOutputStream();

        try {

            byte[] buf = new byte[1024];
            int i=0;

            while ((i = iis.read(buf)) > 0) {
                o.write(buf, 0, i);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return o;
    }
}
