package up6.utils;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import net.sf.json.JSONObject;
import up6.XDebug;
import up6.sql.DataBaseType;
import up6.store.*;
import up6.store.fastdfs.*;
import up6.store.minio.*;
import up6.store.aliyun.*;
import up6.store.obs.*;
import org.apache.commons.lang.StringUtils;

public class ConfigReader {
	public JSONObject m_files;
	public PathTool m_pt;
	public ReadContext m_jp;
	
	public ConfigReader()
	{
		this.m_pt = new PathTool();
		//自动加载/config.json
		String path = this.m_pt.MapPath("/config/config.json");
		String json = FileTool.readAll(path);
		this.m_files = JSONObject.fromObject(json);
		this.m_jp = JsonPath.parse(json);
	}

    public static DataBaseType dbType(){
        ConfigReader r = new ConfigReader();
        String t = r.readString("database.connection.type");

        if(StringUtils.equalsIgnoreCase(t,"mysql")) return DataBaseType.MySQL;
        if(StringUtils.equalsIgnoreCase(t,"postgresql")) return DataBaseType.PostgreSQL;
        if(StringUtils.equalsIgnoreCase(t,"oracle") ) return DataBaseType.Oracle;
        if(StringUtils.equalsIgnoreCase(t,"sql") ) return DataBaseType.SqlServer;
        if(StringUtils.equalsIgnoreCase(t,"dmdb") ) return DataBaseType.DMDB;
        if(StringUtils.equalsIgnoreCase(t,"kingbase") ) return DataBaseType.KingBase;
        if(StringUtils.equalsIgnoreCase(t,"mongodb") ) return DataBaseType.MongoDB;
        return DataBaseType.SqlServer;
    }
    
	/**
	 * 将配置加载成一个json对象
	 * @param name
	 * @return
	 */
	public JSONObject module(String name)
	{
		String path = this.m_jp.read(name);
		XDebug.Output(path);
		path = this.m_pt.MapPath(path);
		String data = FileTool.readAll(path);
		return JSONObject.fromObject(data);
	}
	
	/**
	 * 将json中的值(路径)转换成JsonPath
	 * @param name
	 * @return
	 */
	public ReadContext toPath(String name)
	{
		String path = this.m_jp.read(name);
		path = this.m_pt.MapPath(path);
		String data = FileTool.readAll(path);
		return JsonPath.parse(data);
	}

    /**
     * 将JSON值转换成一个路径
     * @param name
     * @return
     */
    public String readPath(String name)
    {
        Object o = this.m_jp.read(name);
        String v = o.toString();
        v = this.m_pt.MapPath(v);
        return v;
    }

    /**
     * 以JsonPath模式查询数据
     * @param name
     * @return
     */
    public JSONObject read(String name)
    {
        Object o = this.m_jp.read(name);
        return JSONObject.fromObject(o);
    }

    public String readString(String name)
    {
        Object o = this.m_jp.read(name);
        return o.toString();
    }

    /**
     * 以jsonpath方式读取config.json中的bool值
     * @param name
     * @return
     */
    public boolean readBool(String name)
    {
        Object o = this.m_jp.read(name);
        return Boolean.parseBoolean(o.toString());
    }

    public static boolean securityToken() {
		ConfigReader cr = new ConfigReader();
		return cr.readBool("security.token");
    }
    
    /**
     * 获取存储类型
     * @return
     */
    public static StorageType storageType(){
        ConfigReader cr = new ConfigReader();
        String sto  = cr.readString("Storage.type");
        if(sto.equalsIgnoreCase("FastDFS")) return StorageType.FastDFS;
        if(sto.equalsIgnoreCase("Minio")) return StorageType.Minio;
        if(sto.equalsIgnoreCase("OBS") ) return StorageType.OBS;
        if(sto.equalsIgnoreCase("OSS") ) return StorageType.OSS;
        if(sto.equalsIgnoreCase("COS") ) return StorageType.COS;
        return StorageType.IO;
    }

    /**
     * 存储加密
     * @return
     */
    public static boolean storageEncrypt(){
        ConfigReader cr = new ConfigReader();
        return cr.readBool("Storage.encrypt");
    }

    /**
     * 块数据写入器
     * @return
     */
    public static FileBlockWriter blockWriter(){
        FileBlockWriter w = null;
        StorageType st = ConfigReader.storageType();
        if(st==StorageType.FastDFS) w = new FastDFSWriter();
        else if(st==StorageType.Minio) w = new MinioWriter();
        else if(st==StorageType.OBS) w = new ObsWriter();
        else if(st==StorageType.OSS) w = new OSSWriter();
        else  w = new FileBlockWriter();
        return w;
    }

    /**
     * 块数据读取器
     * @return
     */
    public static FileBlockReader blockReader(){
        FileBlockReader w = null;
        StorageType st = ConfigReader.storageType();
        if(st==StorageType.FastDFS) w = new FastDFSReader();
        else if(st==StorageType.Minio) w = new MinioReader();
        else if(st==StorageType.OBS) w = new ObsReader();
        else if(st==StorageType.OSS) w = new OSSReader();
        else  w = new FileBlockReader();
        return w;
    }
}