package up6.utils;

import up6.model.FileInf;

import java.io.ByteArrayOutputStream;

public class Up6Exception extends Exception {
    public Up6Exception(){}
    public Up6Exception(FileInf f, ByteArrayOutputStream b,String msg){
        super(msg);

        String tmp="blockSize：%d\n"
                .concat("blockSizeCpr:%d\n")
                .concat("blockCprType:%s\n")
                .concat("blockSizeCry:%d\n")
                .concat("blockEncryptAgo:%s\n")
                ;
        String s = String.format(tmp,
                f.blockSize,
                f.blockSizeCpr,
                f.blockCprType,
                f.blockSizeSec,
                f.encryptAgo
        );
        System.out.println(s);
    }
    public Up6Exception(String s){
        super(s);
    }
}
