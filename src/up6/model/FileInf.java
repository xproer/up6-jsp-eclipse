package up6.model;

import com.aliyun.oss.model.PartETag;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.obs.services.model.PartEtag;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import up6.biz.FileEtag;
import up6.biz.FolderSchema;
import up6.sql.DataBaseAttribute;
import up6.store.StorageType;
import up6.utils.ConfigReader;
import up6.utils.PathTool;

import java.io.*;
import java.util.*;

/*
 * 原型
 * 更新记录：
 * 	2016-01-07
 * 		FileMD5更名为md5
 * 		PostComplete更名为complete
 * 		FileLength更名为lenLoc
 * 		FileSize更名为sizeLoc
 * 		PostedPercent更名为perSvr
*/
public class FileInf {

	public static FileInf build(){return  new FileInf();}

	public FileInf()
	{		
	}

	@DataBaseAttribute(name="f_id")
	public String id="";
	public String getid(){return this.id;}

	@DataBaseAttribute(name="f_pid")
	public String pid="";
	public String getpid(){return this.pid;}

	@DataBaseAttribute(name="f_pidRoot")
	public String pidRoot="";
	public String getpidRoot(){return this.pidRoot;}

	/**	 * 表示当前项是否是一个文件夹项。	 */
	@DataBaseAttribute(name="f_fdTask")
	public boolean fdTask=false;
	public boolean getfdTask(){return this.fdTask;}

	//	/// 是否是文件夹中的子文件	/// </summary>
	@DataBaseAttribute(name="f_fdChild")
	public boolean fdChild=false;
	public boolean getfdChild(){return this.fdChild;}

	/**	 * 用户ID。与第三方系统整合使用。	 */
	@DataBaseAttribute(name="f_uid")
	public String uid="0";
	public String getuid(){return this.uid;}

	/**	 * 文件在本地电脑中的名称	 */
	@DataBaseAttribute(name="f_nameLoc")
	public String nameLoc="";
	public String getnameLoc(){return this.nameLoc;}

	/**	 * 文件在服务器中的名称。	 */
	@DataBaseAttribute(name="f_nameSvr")
	public String nameSvr="";
	public String getnameSvr(){return this.nameSvr;}

	/**	 * 文件在本地电脑中的完整路径。示例：D:\Soft\QQ2012.exe	 */
	@DataBaseAttribute(name="f_pathLoc")
	public String pathLoc="";
	public String getpathLoc(){return this.pathLoc;}

	/**	 * 文件在服务器中的完整路径。示例：F:\\ftp\\uer\\md5.exe	 */
	@DataBaseAttribute(name="f_pathSvr")
	public String pathSvr="";
	public String getpathSvr(){return this.pathSvr;}

	/**	 * 文件在服务器中的相对路径。示例：/www/web/upload/md5.exe	 */
	@DataBaseAttribute(name="f_pathRel")
	public String pathRel="";
	public String getpathRel(){return this.pathRel;}

	/**	 * 文件MD5	 */
	@DataBaseAttribute(name="f_md5")
	public String md5="";
	public String getmd5(){return this.md5;}

	/**	 * 数字化的文件长度。以字节为单位，示例：120125	 */
	@DataBaseAttribute(name="f_lenLoc")
	public long lenLoc=0;
	public long getlenLoc(){return this.lenLoc;}

	/**
     * 加密后的文件大小
     */
	@DataBaseAttribute(name="f_lenLocSec")
	public long lenLocSec = 0;
	public long getlenLocSec(){return this.lenLocSec;}

	/**	 * 格式化的文件尺寸。示例：10.03MB	 */
	@DataBaseAttribute(name="f_sizeLoc")
	public String sizeLoc="";
	public String getsizeLoc(){return this.sizeLoc;}

	/**	 * 文件续传位置。	 */
	public long offset=0;
	public long getoffset(){return this.offset;}

	/**	 * 已上传大小。以字节为单位	 */
	@DataBaseAttribute(name="f_lenSvr")
	public long lenSvr=0;
	public long getlenSvr(){return this.lenSvr;}

	/**	 * 已上传百分比。示例：10%	 */
	@DataBaseAttribute(name="f_perSvr")
	public String perSvr="0%";
	public String getperSvr(){return this.perSvr;}

	@DataBaseAttribute(name="f_complete")
	public boolean complete=false;
	public boolean getcomplete(){return this.complete;}

	@DataBaseAttribute(name="f_time")
	public Date time = new Date();
	public Date gettime(){return this.time;}

	@DataBaseAttribute(name="f_deleted")
	public boolean deleted=false;
	public boolean getdeleted(){return this.deleted;}

	/**	 * 是否已经扫描完毕，提供给大型文件夹使用，大型文件夹上传完毕后开始扫描。	 */
	@DataBaseAttribute(name="f_scan")
	public boolean scaned=false;
	public boolean getscaned(){return this.scaned;}

	//块索引，基于1
	@DataBaseAttribute(name="f_blockIndex")
	public int blockIndex=0;
	public int getblockIndex(){return this.blockIndex;}

	//块偏移,基于整个文件
	@DataBaseAttribute(name="f_blockOffset")
	public long blockOffset=0;
	public long getblockOffset(){return this.blockOffset;}

	//块加密偏移
	public long blockOffsetCry=0;

	@DataBaseAttribute(name="f_blockSize")
	public int blockSize=0;
	public int getblockSize(){return this.blockSize;}
	
	@DataBaseAttribute(name="f_blockSizeSec")
	public int blockSizeSec=0;
	public int getblockSizeSec(){return this.blockSizeSec;}

	//块压缩大小
	public int blockSizeCpr=0;
	//块压缩算法,gzip,zip
	public String blockCprType="";

	/// <summary>
	/// 块是否加密
	/// </summary>
	public Boolean blockEncrypt= false;

	//文件块总数
	@DataBaseAttribute(name="f_blockCount")
	public int blockCount=0;
	public int getblockCount(){return this.blockCount;}

	//加密存储
	@DataBaseAttribute(name="f_encrypt")
	public boolean encrypt=false;
	public boolean getencrypt(){return this.encrypt;}
	
	@DataBaseAttribute(name="f_encryptAgo")
	public String encryptAgo = "";
	public String getencryptAgo(){return this.encryptAgo;}

	//用于第三方存储的对象ID，如FastDFS,Minio等
	public String object_id="";

	@DataBaseAttribute(name="f_object_key")
	public String object_key="";
	public String getobject_key(){return this.object_key;}

	public Map<String,String> fields = new HashMap<String,String>();
	
	//
	public String etag="";

	public String schemaFile() {
		return this.parentDir().concat("/schema.txt");
	}
	
	/**
	 * 生成AWS S3文件key
	 * D:\Soft\guid\QQ.exe => /guid/QQ.exe
	 * @return
	 */
	public String S3Key() {    	
    	//格式 /guid/QQ.exe
    	String key = PathTool.combin("", this.id);
    	key = PathTool.combin(key, this.nameLoc);
    	return key;
	}
	
	/**
     * 	生成OBS key
     * D:\Soft\guid\QQ.exe => guid/QQ.exe
     * @return
     */
    public String ObsKey(){
        //格式 guid/QQ.exe
        String key = PathTool.combin(this.id, this.nameLoc);
        return key;
    }

	public String ObjectKey(){
		//Minio key => /dir/filename
		if(ConfigReader.storageType()== StorageType.Minio)return this.S3Key();
		else if(
			//oss key => dir/filename
				ConfigReader.storageType() == StorageType.OBS||
						ConfigReader.storageType() == StorageType.OSS||
						ConfigReader.storageType() == StorageType.COS
		)
			return this.ObsKey();
		return  this.object_id;
	}
	
	public String ObjectID() {
		//Minio key => /dir/filename
		if (ConfigReader.storageType() == StorageType.Minio) return this.S3Key();
		else if(
                //oss key => dir/filename
                ConfigReader.storageType() == StorageType.OBS||
                ConfigReader.storageType() == StorageType.OSS)
            return this.ObsKey();
        return this.object_id;
	}
	
	/**
	 * 返回AWS s3，ETag保存文件
	 * 路径：dir/guid/etags.txt
	 * @return
	 */
	public String ETagsFile() {
		String f = PathTool.combin(this.parentDir(),this.id);
		f = PathTool.combin(f,"etags.txt");
		return f;
	}

	public String parentDir()
	{
		File f = new File(this.pathSvr);
		return f.getParent().replace('\\', '/');
	}
	
	public String relDir()
	{
		File f = new File(this.pathRel);
		return f.getParent();
	}

	//保存到层级信息文件中
	public void saveScheme()
	{
		//System.out.println("FileInf.saveScheme,块索引："+String.valueOf(this.blockIndex));
		//仅第一块保存数据
		if (1 != this.blockIndex ) return;

		//不是子文件=>退出
		if (StringUtils.isEmpty(this.pid) ) return;

		//保存子文件数据
		FolderSchema fs = new FolderSchema();
		fs.addFile(this);
	}
	
	/**
	 * 保存块ID信息
	 * 路径：
	 * D:/Soft/guid/etags.txt
	 */
	public void saveEtags() {
		FileEtag fe = new FileEtag();
		fe.saveTags(this);
	}
	
	/**
	 * <CompleteMultipartUpload xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
	   <Part>
	      <ChecksumCRC32>string</ChecksumCRC32>
	      <ChecksumCRC32C>string</ChecksumCRC32C>
	      <ChecksumSHA1>string</ChecksumSHA1>
	      <ChecksumSHA256>string</ChecksumSHA256>
	      <ETag>string</ETag>
	      <PartNumber>integer</PartNumber>
	   </Part>
	   ...
	</CompleteMultipartUpload>

	 * @return
	 */
	public String etags() {
		String xml = "";
		List<FileInf> files = new ArrayList<FileInf>();
		//加载/guid/etags.txt
		File f = new File(this.ETagsFile());
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF-8"));
			String line = null;
			Gson g = new Gson();
			Map<Integer,Boolean> blocks = new HashMap<Integer, Boolean>();
			while( (line = br.readLine() )!=null )
			{
				FileInf block = g.fromJson(line, FileInf.class);
				if(!blocks.containsKey(block.blockIndex))
				{
					files.add(block);
					blocks.put(block.blockIndex,true);
				}
			}
			br.close();
			
		    Document dom = DocumentHelper.createDocument();
		    Element root = dom.addElement("CompleteMultipartUpload");
		    for(FileInf i : files)
		    {
		    	Element part = root.addElement("Part");
		    	part.addElement("ETag").addText(i.etag);
		    	part.addElement("PartNumber").addText( String.valueOf( i.blockIndex ));
		    }
		    xml = root.asXML();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}
	
	/**
     * 	获取华为对象存储
     * @return
     */
    public List<PartEtag> etagsObs(){
        List<PartEtag> parts = new ArrayList<PartEtag>();
        List<FileInf> files = new ArrayList<FileInf>();
        //加载/guid/etags.txt
        File f = new File(this.ETagsFile());
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF-8"));
            String line = null;
            Gson g = new Gson();
            Map<Integer, Boolean> blocks = new HashMap<Integer, Boolean>();
            while( (line = br.readLine() )!=null )
            {
                FileInf block = g.fromJson(line, FileInf.class);
                //防止重复添加块信息
                if(!blocks.containsKey(block.blockIndex))
                {
                    files.add(block);
                    blocks.put(block.blockIndex,true);
                }
            }
            br.close();

            for(FileInf i : files)
            {
                PartEtag part = new PartEtag();
                part.setPartNumber(i.blockIndex);
                part.setEtag(i.etag);
                parts.add(part);
            }

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return parts;
    }
	
	public List<PartETag> etagsOSS(){
        List<PartETag> parts = new ArrayList<PartETag>();
        List<FileInf> files = new ArrayList<FileInf>();
        //加载/guid/etags.txt
        File f = new File(this.ETagsFile());
        BufferedReader br;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF-8"));
            String line = null;
            Gson g = new Gson();
            Map<Integer, Boolean> blocks = new HashMap<Integer, Boolean>();
            while( (line = br.readLine() )!=null )
            {
                FileInf block = g.fromJson(line, FileInf.class);
                //防止重复添加块信息
                if(!blocks.containsKey(block.blockIndex))
                {
                    files.add(block);
                    blocks.put(block.blockIndex,true);
                }
            }
            br.close();

            for(FileInf i : files)
            {
                PartETag part = new PartETag(i.blockIndex,i.etag);
                parts.add(part);
            }

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return parts;
    }
	
	public String toJson() {
		Gson g = new Gson();
		return g.toJson(this);
	}

	public String formatSize(long byteCount)
	{
		return PathTool.BytesToString(byteCount);
	}
}