package up6.model;

import up6.sql.DataBaseAttribute;

import java.util.UUID;

public class DnFileInf extends  up6.model.FileInf
{
	public static DnFileInf build(){return  new DnFileInf();}
	public DnFileInf(){}

	public String makeID(){
		String guid = UUID.randomUUID().toString();
		guid = guid.replace("-", "");
		return guid;
	}
	
	public String f_id = "";
	@DataBaseAttribute(name="f_fileUrl")
	public String fileUrl = "";
	@DataBaseAttribute(name="f_sizeSvr")
	public String sizeSvr = "0byte";
	@DataBaseAttribute(name="f_perLoc")
	public String perLoc = "0%";
	//远程文件加密大小
	public long lenSvrSec = 0;
	public long getlenSvrSec(){return this.lenSvrSec;}
}
