package up6.store;

import up6.utils.PathTool;
import up6.model.FileInf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileBlockWriter {
	public StorageType storage;//写入器类型
	
	public FileBlockWriter()
	{
		this.storage = StorageType.IO;
	}
	
	/**
	 * 参数：pathSvr,lenLoc
	 * @param file
	 * @return
	 */
	public String make(FileInf file) throws IOException {
		File ps = new File(file.pathSvr);
		PathTool.createDirectory(ps.getParent());
		
	    RandomAccessFile raf = new RandomAccessFile(file.pathSvr, "rw");
	    raf.setLength(file.lenLoc);//fix:以原始大小创建文件
		if(file.encrypt) raf.setLength(file.lenLocSec);
	    raf.close();
	    return "";
	}
	
	/**
	 * 参数：pathSvr,blockOffset,blockIndex
	 * @param file
	 * @param ostm
	 */
	public String write(FileInf file,ByteArrayOutputStream ostm) throws IOException {		
		if (!PathTool.exist(file.pathSvr))
			throw new IOException("文件不存在:"+file.pathSvr);
		
		byte[] data = ostm.toByteArray();
		
		//bug:在部分服务器中会出现错误：(另一个程序正在使用此文件，进程无法访问。)
		RandomAccessFile raf = new RandomAccessFile(file.pathSvr,"rw");
		//使用块加密偏移
		if(file.encrypt) raf.seek(file.blockOffsetCry);
		else raf.seek(file.blockOffset);
		raf.write(data);
		raf.close();
		return "ok";
	}

	/**
	 * 写入最后一块数据=>所有文件块上传完毕
	 * @param file
	 * @return
	 */
	public boolean writeLastPart(FileInf file) throws IOException {
		return  true;
	}
}
