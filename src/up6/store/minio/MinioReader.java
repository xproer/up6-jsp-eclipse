package up6.store.minio;

import up6.utils.PathTool;
import up6.store.*;

public class MinioReader extends FileBlockReader{
	public MinioReader() {
		this.storage = StorageType.Minio;
	}

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size)
    {
    	//还原成汉字，由okhttp自动转码。
    	pathSvr = PathTool.url_decode(pathSvr);
        return MinioTool.getObject(pathSvr,  offset, size);
    }
}
