package up6.store.aliyun;

import up6.utils.PathTool;
import up6.store.FileBlockReader;
import up6.store.StorageType;

public class OSSReader extends FileBlockReader {
    public OSSReader() {
        this.storage = StorageType.OSS;
    }

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size) throws Exception {
        //还原成汉字，由okhttp自动转码。
        pathSvr = PathTool.url_decode(pathSvr);
        return OSSTool.getObject(pathSvr,  offset, size);
    }
}
