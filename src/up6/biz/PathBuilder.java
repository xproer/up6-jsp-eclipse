package up6.biz;

import org.apache.commons.lang.StringUtils;
import up6.utils.ConfigReader;
import up6.utils.PathTool;
import up6.model.FileInf;

import java.io.IOException;


public class PathBuilder {
	
	public PathBuilder(){}
	
	/**
	 * 获取上传路径
	 * 格式：
	 * 	webapp_name/upload
	 * @return dir/
	 * @throws IOException
	 */
	public String getRoot() throws IOException
	{
		//前面会多返回一个"/", 例如  /D:/test/WEB-INF/, 奇怪, 所以用 substring()
		//path：D:/app/tomcat/apache-tomcat-8.5.61/webapps/up6/WEB-INF/classes/
	    PathTool pt = new PathTool();
		String root = pt.getRoot();

		ConfigReader cr = new ConfigReader();
		String pathSvr = cr.readString("IO.dir");
		pathSvr = pathSvr.replace("{root}", root);
		pathSvr = pathSvr.replaceAll("\\\\", "/");
		return pathSvr.trim();
	}
	public String genFolder(FileInf fd) throws IOException{return "";}
	public String genFile(String uid,FileInf f) throws IOException{return "";}
	public String genFile(String uid,String md5,String nameLoc)throws IOException{return "";}
	/**
	 * 相对路径转换成绝对路径
	 * 格式：
	 * 	/2021/05/28/guid/nameLoc => d:/upload/2021/05/28/guid/nameLoc
	 * @return
	 * @throws IOException
	 */
	public String relToAbs(String path) throws IOException
	{
		String root = this.getRoot();
		root = root.replaceAll("\\\\", "/");
		path = path.replaceAll("\\\\", "/");
		if(!path.startsWith(root))
		{
			path = PathTool.combine(root, path);
		}
		return path.trim();
	}
	/**
	 * 将路径转换成相对路径
	 * @return
	 * @throws IOException
	 */
	public String absToRel(String path) throws IOException
	{
		String root = this.getRoot().replaceAll("\\\\", "/");
		path = path.replaceAll("\\\\", "/");
		path = path.replaceAll(root, "");
		return path.trim();
	}
}
