package up6.biz;

import up6.utils.FileTool;
import up6.utils.PathTool;
import up6.model.FileInf;

import java.io.File;

import com.google.gson.Gson;

public class FolderSchema {

    public FolderSchema() {}

    /**
     * 创建目录信息
     * pathSvr/folder.txt
     * 结构：
     * folder,id,pid,pidRoot,pathSvr,pathRel,lenLoc
     * @param dir
     */
    public boolean create(FileInf dir)
    {
        String file = dir.schemaFile();
        Gson g = new Gson();
        String val = g.toJson(dir);
        return FileTool.writeAll(file, val+"\n");
    }

    /**
     * 向层级结构信息文件添加一条记录
     * dir/schema.txt
     * @param f
     */
    public void addFile(FileInf f) {
    	//pathSvr：D:/tomcat/webapps/up6/upload/2022/05/30/dc32a3f39dc7473da86f3b316dfce346/test/test1/test2/memo.txt
    	//pathRel：test1/test2/memo.txt
        String root = f.pathSvr.replace(f.pathRel, "");
        //root：D:/tomcat/webapps/up6/upload/2022/05/30/dc32a3f39dc7473da86f3b316dfce346/test/
        root = PathTool.getParent(root);
        //file：D:/tomcat/webapps/up6/upload/2022/05/30/dc32a3f39dc7473da86f3b316dfce346/schema.txt
        String file = PathTool.combin(root , "schema.txt");
        Gson g = new Gson();
        String val = g.toJson(f);

        //结构文件不存在
        File fe = new File(file);
        if(!fe.exists()) return;

        FileTool.appendLine(file, val);
    }
}